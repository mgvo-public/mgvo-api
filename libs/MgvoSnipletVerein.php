<?php
/** @noinspection PhpUnused */

// In der Datei wird eine Sniplet-Klasse definiert, welche vereinsspezifische Besonderheiten darstellt.
// Hier sollten alle abweichenden Änderungen vorgenommen werden. Funktionen, die hier nicht definiert sind, werden aus der parent-Klasse übernommen.
// bei Bedarf kann die parent-Funktion auch aus der Funktion hier aufgerufen werden.
// Das Wordpress-Plugin prüft erst, ob die Klasse MGVO_SNIPLET_VEREIN vorhanden ist und verwendet diese dann, wenn nein, wird die Klasse MGVO_SNIPLET direkt verwendet.
// Dies kann auch ohne Wordpress (z.B. mit Joomler, Contao, eigener PHP-Lib etc.) integriert werden.
// Für eine Contao-Integration könnten sie sich auch an den Support wenden, hier gibt es einen Verein, der dies bereits durchgeführt hat.

class MgvoSnipletVerein extends MgvoSniplet {
   
   
    function __construct(string $call_id,  $vcryptkey = "", int $cachemin = 5) {
        parent::__construct($call_id, $vcryptkey, $cachemin);
    }

    /*
    * Diese Funktion ruft ihre parent-Funktion auf.
    */
    function mgvo_sniplet_orte($a = []): string {
        $html = "";
        //$html .= "Ausgabe vor dem Sniplet";
        $html .= parent::mgvo_sniplet_orte($a);
        //$html .= "Ausgabe nach dem Sniplet";
        return $html;
    }

    /**
     * Gibt eine Tabelle mit den Gruppen zurück
     *
     * @param ?array $filter Filter, wie sie z.B. von Wordpress Shortcodes als $attr geliefert werden. Siehe mgvo_filter_gruppen()
     *
     * Diese Funktion ist aus mgvo_sniplets.php kopiert. Wenn sie Anpassungen vornehmen wollen,
     * kommentierten sie die Funktion hier ein und führen ihre Änderungen durch.
     */
    function mgvo_sniplet_gruppen(?array $filter = null, ?string $gruppenlink = null): string {
        return parent::mgvo_sniplet_gruppen($filter, $gruppenlink);
        /*
        $resar = $this->api->read_gruppen();
        $ar = $this->mgvo_filter_gruppen($resar['objar'], $filter);
        $sniplet = "<div class='mgvo mgvo-gruppen'>";
        $sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<table class='mgvo-gruppen'>";
        $sniplet .= "<tr>";
        //$sniplet .= "<th>Gruppen-ID</th>";
        $sniplet .= "<th>Name</th>";
        $sniplet .= "<th>Betreuer</th>";
        $sniplet .= "</tr>";
        $this->api->api_debug(__FUNCTION__, "Gruppenlink: ".$gruppenlink);
        foreach($ar as $or) {
            $sniplet .= "<tr>";
            //$sniplet .= "<td>".$or['gruid']."</td>";
            if (!empty($gruppenlink)) {
                $url= $gruppenlink."/?gruid=".$or['gruid'];
                $sniplet .= "<td><a href='".$url."'>".$or['grubez']."</a></td>";
            } else {
                $sniplet .= "<td>".$or['grubez']."</td>";
            }
            $sniplet .= "<td>";
            $sniplet .= isset($or['trnameall']) ?  $or['trnameall'] : "";
            $sniplet .= "</td>";
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";
        $this->api->api_printdebug_hidden();
        return $sniplet;
        */
    }
}
