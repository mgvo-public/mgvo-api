
<?php 

      echo "<style>";
      echo ".vcbwb_hinweis { font-size: .8rem }";

		if($this->loadMitantragFiles){ // CSS wird nur bei stand-alone-Nutuzung der sniplets eingebunden. Sonst wird dies im Wordpress-Plugin über WP-enquire vorgenommen.
				include(__DIR__.'/mitantrag.css'); 
			}
			echo "</style>";
?>
		
		<div class="container" id="mg-antrag">
			<form name="mitedit" id="mitedit" method="post" action="<?php echo $url ?>"  class="needs-validation" novalidate>
					<div class="anschrift">
						<h3>Anschrift</h3>
						<div class="row">
							<div class="col">
								<label for="anrede">Anrede</label> <br>
								<select name="anrede" class="form-control" id="anrede" required>
									<option value="" selected></option>
									<option value="1">Herr</option>
									<option value="2">Frau</option>
									<option value="3">Herr Dr.</option>
									<option value="4">Frau Dr.</option>
									<option value="5">Herr Prof. Dr.</option>
									<option value="6">Frau Prof. Dr.</option>
									<option value="7">Firma</option>
									<option value="8">Herr Mag.</option>
									<option value="9">Frau Mag.</option>
									<option value="10">Herr Mag. Dr.</option>
									<option value="11">Frau Mag. Dr.</option>
									<option value="12">Herr Mag. (FH)</option>
									<option value="13">Frau Mag. (FH)</option>
									<option value="14">Herr Ing.</option>
									<option value="15">Frau Ing.</option>
									<option value="16">Herr Dipl. Ing.</option>
									<option value="17">Frau Dipl. Ing.</option>
									<option value="18">Herr DDr.</option>
									<option value="19">Frau DDr.</option>
									<option value="20">keine Anrede</option>
									<option value="21">Herr und Frau</option>
									<option value="22">Familie</option>
									<option value="23">Herr Prof.</option>
									<option value="24">Frau Prof.</option>
								</select>
								<div class="invalid-feedback">
									Wie sollen wir Sie ansprechen?
								</div>
							</div>
							<div class="col">
								<label for="geschlecht">Geschlecht</label> <br>
								<select name="geschlecht" class="form-control" id="geschlecht" required>
									<option value="" selected></option>
									<option value="m">Männlich</option>
									<option value="w">Weiblich</option>
									<option value="s">sächlich (Firma)</option>
								</select>
								<div class="invalid-feedback">
									Bitte Feld ausfüllen.
								</div>
							</div>
						</div>
						<div class="row">            
							<div class="col">
								<label for="vorname">Vorname</label>
								<input type="text" class="form-control" placeholder="Vorname" name="vorname" id="vorname" required/>
								<div class="invalid-feedback">
									Bitte geben Sie Ihren Vornamen ein
								</div>
							</div>
							<div class="col">
								<label for="nachname">Nachname</label>
								<input type="text" class="form-control" placeholder="Nachname" name="nachname" id="nachname" required>
								<div class="invalid-feedback">
									Bitte geben Sie Ihren Nachnamen an.
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label for="gebdat">Geburtsdatum</label> <br>
								<input type="date" name="gebdat" class="form-control" id="gebdat" required/>
								<div class="invalid-feedback">
									Bitte geben Sie Ihr Geburtsdatum ein.
								</div>
							</div>
						</div>
						<?php if($config["showAddress"]){ ?>
							<div class="row">
								<div class="col">
									<label for="str">Straße</label> <br>
									<input type="text" name="str" placeholder="Straße" class="form-control" id="str" required/>
									<div class="invalid-feedback">
										Bitte geben Sie Ihre Straße an.
									</div>
								</div>
							</div>
							<div class="row">
									<div class="col">
										<label for="plz">Postleitzahl</label> <br>
										<input type="text" name="plz" placeholder="Postleitzahl" class="form-control" id="plz" required/>
										<div class="invalid-feedback">
											Bitte geben Sie Ihre Postleitzahl an.
										</div>
									</div>
									<div class="col">
										<label for="ort">Ort</label> <br>
										<input type="text" name="ort" placeholder="Ort" class="form-control" id="ort" required/>
										<div class="invalid-feedback">
											Bitte geben Sie Ihren Ort an.
										</div>
									</div>
							</div>
						<?php } ?>
						<?php if($config["showTel"]){ ?>
						<div class="row">
							<div class="col">
								<label for="tel1">Telefon</label>
								<input type="tel" name="tel1" placeholder="Telefon 1" class="form-control" id="tel1">
							</div>
							<?php if($config["showTel"] && $config["showSecondTel"]){ ?>
							<div class="col">
								<label for="tel2">Telefon 2</label>
								<input type="tel" name="tel2" placeholder="Telefon 2" class="form-control" id="tel2">
							</div>
							<?php } ?>
						</div>
						<?php } ?>
						<?php if($config["showMobile"]){ ?>
						<div class="row">
							<div class="col">
								<label for="mobil1">Mobiltelefon</label>
								<input type="tel" name="mobil1" placeholder="Mobiltelefon 1" class="form-control" id="mobil1">
							</div>
							<?php if($config["showMobile"] && $config["showSecondMobile"]){ ?>
							<div class="col">
								<label for="mobil2">Mobiltelefon 2</label>
								<input type="tel" name="mobil2" placeholder="Mobiltelefon 2" class="form-control" id="mobil2">
							</div>
							<?php } ?>
						</div>
						<?php } ?>
						<?php if($config["showEmail"]){ ?>
						<div class="row">
							<div class="col">
								<label for="email">E-Mail</label>
								<input type="email" name="email" placeholder="E-Mail" class="form-control" id="email" required>
								<div id="emailFeedback" class="invalid-feedback">
									Bitte geben Sie Ihre E-Mail an.
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
					<?php if($config["showPayment"]){ ?>
					<div class="zahlungsdaten">
						<h3>Zahlungsdaten</h3>
						<div class="row">
							<div class="col">
								<label for="kontoname">Zahler</label>
								<input type="text" name="kontoname" placeholder="Zahler" class="form-control" id="kontoname">
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label for="iban">IBAN</label>
								<input type="text" name="iban" placeholder="IBAN" class="form-control" id="iban">
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label for="bic">BIC</label>
								<input type="text" name="bic" placeholder="BIC" class="form-control" id="bic">
								<small class="form-text text-muted">
									Die BIC ben&ouml;tigen wir nur bei ausl&auml;ndischen Konten.
								</small>
							</div>
							<?php if($config["showPayment"] && $config["showFormOfPayment"]){ ?>
							<div class="col">
								<label for="zahlweise">Zahlweise</label>
								<select name="zahlweise" class="form-control" id="zahlweise" required>
		<!--  							<option value="v" selected>vierteljährlich</option> -->
									<option value="h" selected>halbjährlich</option>
				<!--  					<option value="j">jährlich</option> -->
								</select>
								<div class="invalid-feedback">
									In welchen Intervallen sollen wir den Beitrag einziehen?
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="row d-none">
							<div class="col">
								<label for="zahlungsart">Zahlungsart</label>
								<select name="zahlungsart" class="form-control" id="zahlungsart">
									<option value="l" selected>Lastschrift</option>
								</select>
							</div>
						</div>
						<div class="row">
						<?php if($config["showPayment"] && $config["showReducedFee"]){ ?>
							<div class="col-12 col-md-6  d-flex py-3">
								<input class="col-2" type="checkbox" class="form-control" id="ermaessigung">
                        <label for="ermaessigung" class="col-10 mb-0 px-0">Erm&auml;&szlig;igter Beitrag (Student / in Ausbildung)</label>
								
							</div>
						<?php } ?>

						</div>
						<div class="row">
							<div class="col-12 col-md-6 d-flex py-3">
								<input class="col-2" type="checkbox" name="eev_conf" class="form-control" id="eev_conf" value="1">
								<label for="eev_conf" id="sepa-label" class="col-10 mb-0 px-0">SEPA-Mandatserteilung (Dieser muss zugestimmt werden)</label>
							</div>
							<p id="sepa-hint">
							Ich ermächtige den Verein, Zahlungen von meinem Konto mittels Lastschrift einzuziehen. Zugleich weise ich mein Kreditinstitut an, die von Verein auf mein Konto gezogenen Lastschriften einzulösen.
									Hinweis: Ich kann innerhalb von acht Wochen, beginnend mit dem Belastungsdatum, die Erstattung des belasteten Betrages verlangen. Es gelten dabei die mit meinem Kreditinstitut vereinbarten Bedingungen.
							</p>
						</div>
					</div>
					<?php } ?>
					
	

										
			 	<?php if($config["showZusatzfelder"]) { ?>
					<div class="zusatzfelder">
						<h3 class="mb-2">Zusätzliche Angaben</h3>
	
						<?php 
						for ($i = 1 ; $i<=16; $i++) {
						   $zfdl_id = 'zfld'.( $i < 10 ? "0":"").$i;
						   if ($config["zfld"][$i] == true) {
						      echo '<div class="row">';
						      echo    '<div class="col">';
						      if ($i == 1) {
						         echo       '<label for="'.$zfdl_id.'">E-Mail Erziehungsberechtigte'.$i.'</label>';
						         echo       '<input type="text" name="'.$zfdl_id.'" placeholder="example@email.com" class="form-control" id="'.$zfdl_id.'">';		
						      } else 
						      if ($i == 13) {
						         echo '<label >Konfektionsgröße (T-Shirt)</label><br>';
						         echo '<select class="form-select" aria-label="Default select example" id="'.$zfdl_id.'" name="'.$zfdl_id.'">';
						         echo '<option selected></option>';
						         echo '<option value="XS">XS</option>';
						         echo '<option value="S">S</option>';
						         echo '<option value="M">M</option>';
						         echo '<option value="L">L</option>';
						         echo '<option value="XL">XL</option>';
						         echo '<option value="XXL">XXL</option>';
						         echo '<option value="XXXL">XXXL</option>';
						         echo '</select><br><br>';					         
						      } else {
						         if ($i == 14) {
						            echo '<div class="form-check">';
						            echo    '<input class="form-check-input" type="checkbox" value="" id="'.$zfdl_id.'" name="'.$zfdl_id.'">';
						            echo    '<label class="form-check-label" for="'.$zfdl_id.'">';
						            echo    "Hiermit erkenne ich die Satzung und Ordnungen des VC Blau-Weiß Brandenburg e.V. und die nachfolgende Datenschutzerklärung an.
Die Satzung und Ordnungen sind auf unserer Homepage einzusehen.";

						            echo    '</label>';
						            echo '</div>';
						            ?>
						            <p class="datenschutztext vcbwb_hinweis" >
						            Datenschutz / Persönlichkeitsrechte  

Falls mein Aufnahmeantrag angenommen wird und ich Mitglied des Volleyball Clubs Blau – Weiß Brandenburg e.V. (VCB) werde, bin ich mit der Erhebung, Verarbeitung (Speicherung, Veränderung, Übermittlung) oder Nutzung meiner personenbezogenen Daten in dem folgenden Ausmaß einverstanden: <br>
 </p><p class="datenschutztext vcbwb_hinweis">
1. Der Verein erhebt, verarbeitet und nutzt personenbezogene Daten seiner Mitglieder unter  Einsatz von Datenverarbeitungsanlagen (EDV) zur Erfüllung der gemäß der Satzung zulässigen Zwecke und Aufgaben, beispielsweise im Rahmen der Mitgliederversammlung.    Hierbei handelt es sich insbesondere um folgende Mitgliederdaten:  (bei Minderjährigen die Angaben der Erziehungsberechtigten) <br>
<ul class="vcbwb_hinweis" >
<li>Name, Vorname</li>
<li>Geburtsdatum</li>
<li>Anschrift</li>
<li>Bankverbindung</li>
<li>E-Mailadressen</li>
<li>Lizenznummern von Spielerpass, Schiedsrichterpass und Übungsleiterpass</li>
<li>Mannschaftszugehörigkeiten</li>
<li>Funktionen im VCB/SSB/BVV/LSB </li>
<li>Ehrungen</li>
</ul>
 </p><p  class="datenschutztext vcbwb_hinweis">
2.  Als Mitglied des Stadtsportbundes Brandenburg e.V. (SSB), des Brandenburgischen Volleyballverbandes e.V (BVV) und des Landessportbund Brandenburg e.V. (LSB) ist der Volleyball Club Blau – Weiß Brandenburg e.V. verpflichtet bestimmte personenbezogene  Daten dorthin zu melden.   Übermittelt werden an
<table style="max-width: 250px;" class="vcbwb_hinweis" >
<tr><th>Attribut</th><th>SSB</th><th>BVV</th><th>LSB</th></tr>  
<tr><td>Name Vorname</td><td>X</td><td>  X</td><td>  X</td></tr>
<tr><td>Anschrift  </td><td>X</td><td>  X</td><td>  X</td></tr>
<tr><td>Funktionen</td><td>X</td><td>  X</td><td>  X</td></tr>
<tr><td>Lizenznummern/td><td>X</td><td>  X</td><td>  X</td></tr>
<tr><td>Mannschaftszugehörigkeit /td><td>X</td><td>  X</td><td>  X</td></tr>
<tr><td>Ehrungen</td><td>X</td><td>  X</td><td>  X</td></tr>
</table>
 </p><p class="datenschutztext vcbwb_hinweis">
3.  Der Verein/LSB hat Versicherungen abgeschlossen bzw. schließt solche ab, aus denen er  und/oder seine Mitglieder Leistungen beziehen können. Soweit dies zur Begründung,  Durchführung oder Beendigung dieser Verträge erforderlich ist, übermittelt der Verein/LSB  personenbezogene Daten seiner Mitglieder    - Name    - Anschrift    - Geburtsdatum    - Funktion im Verein/Verband    - KFZ-Kennzeichen  an das zuständige Versicherungsunternehmen. Der Verein/LSB stellt hierbei vertraglich  sicher, dass der Empfänger der Daten diese ausschließlich gemäß dem Übermittlungszweck  verwendet.<br>  
 </p><p class="datenschutztext vcbwb_hinweis">
4.  Im Zusammenhang mit seinen Aufgaben bzw. seinem Verbandszweck veröffentlicht der VCB/SSB/BVV/LSB personenbezogene Daten und Fotos seiner Mitglieder  in  Print- und Telemedien sowie elektronische Medien.    - Name    - Geburtsdatum    - Mannschaftszugehörigkeit    - Ehrungen    - Fotos Ein Mitglied kann jederzeit gegenüber dem Vorstand der Veröffentlichung von Einzelfotos seiner Person widersprechen. Ab Zugang des Widerspruchs unterbleibt die Veröffentlichung/Übermittlung und der VCB/SSB/BVV/LSB entfernt vorhandene Fotos von seiner Homepage.<br>  
 </p><p  class="datenschutztext vcbwb_hinweis">
5.  In seiner VCB/SSB/BVV/LSB-Zeitschrift sowie auf seiner Homepage berichtet der VCB/SSB/BVV/LSB auch über Ehrungen, Zugehörigkeitszeiten und Geburtstage seiner Mitglieder. Hierbei werden Fotos von Mitgliedern und andere personenbezogene Daten veröffentlicht.    - Name     - Geburtsdatum    - Fotos    - Wohnort    - Zugehörigkeitszeiten    - Ehrungen   Berichte über Ehrungen, nebst Foto, darf der VCB/SSB/BVV/LSB unter Meldung von Namen und Funktion im VCB/SSB/BVV/LSB auch an andere Print- und Telemedien sowie elektronische Medien übermitteln.   Im Hinblick auf Ehrungen und Geburtstage kann das betroffene Mitglied jederzeit gegenüber dem Vorstand der Veröffentlichung/Übermittlung von Einzelfotos sowie seiner personenbezogenen Daten allgemein oder für einzelne Ereignisse widersprechen.  Der VCB/SSB/BVV/LSB informiert das Mitglied rechtzeitig über eine beabsichtigte Veröffentlichung/Übermittlung in diesem Bereich und teilt hierbei auch mit, bis zu welchem Zeitpunkt ein Widerspruch erfolgen kann. Wird der Widerspruch fristgemäß ausgeübt, unterbleibt die Veröffentlichung/Übermittlung. Anderenfalls entfernt der VCB/SSB/BVV/LSB Daten und Einzelfotos des widersprechenden Mitglieds von seiner Homepage und verzichtet auf künftige Veröffentlichungen/Übermittlungen.<br>
 </p><p class="datenschutztext vcbwb_hinweis">
6. Mitgliederlisten werden als Datei oder in gedruckter Form soweit an Vorstandsmitglieder,  sonstige Funktionäre und Mitglieder herausgegeben, wie deren Funktion oder besondere  Aufgabenstellung im VCB/SSB/BVV/LSB die Kenntnisnahme erfordern.   Macht ein Mitglied glaubhaft, dass es die Mitgliederliste zur Wahrnehmung seiner satzungsmäßigen Rechte (z.B. Minderheitenrechte) benötigt, wird ihm oder einem Treuhändler eine gedruckte Kopie der notwendigen Daten (bzw. eine Kopie der notwendigen Daten auf Datenträger) gegen schriftliche Versicherung ausgehändigt, dass Namen, Adressen und sonstige Daten nicht zu anderen Zwecken Verwendung finden und  die erhaltenen Daten sobald deren Zweck erfüllt ist, zurückgegeben, vernichtet oder gelöscht werden.<br>  
 </p><p class="datenschutztext vcbwb_hinweis">
7. Jedes Mitglied hat im Rahmen der gesetzlichen Vorschriften des Bundesdatenschutzgesetzes (BDSG) (insbesondere §§34 und 35) das Recht auf Auskunft über die zu seiner Person gespeicherten Daten, deren Empfänger und den Zweck der Speicherung sowie auf Berichtigung, Löschung oder Sperrung seiner Daten. Das Recht auf Widerspruch gegen die Verarbeitung der personenbezogenen Daten gemäß §36 BDSG kann von jedem Mitglied zu jeder Zeit ausgeübt werden.   Eine anderweitige, über die Erfüllung seiner satzungsgemäßen Aufgaben hinausgehende  Datenverarbeitung oder Nutzung der Daten ist dem VCB/SSB/BVV/LSB nur erlaubt, sofern  er aus gesetzlichen Gründen hierzu verpflichtet ist oder das Mitglied eingewilligt hat.  Ein Datenverkauf ist nicht statthaft.<br>
 </p>
						            <?php
						         } else {					      
      						      echo       '<label for="'.$zfdl_id.'">Zusatzfeld'.$i.'</label>';
      						      echo       '<input type="text" name="'.$zfdl_id.'" placeholder="Zusatzfeld" class="form-control" id="'.$zfdl_id.'">';
						         }
						      }
						      echo    '</div>';
						      echo '</div>';
						   }	
						}				
						?>
		
					</div>
					<?php } ?>
					
					<?php if($config["showOther"]) { ?>
					<div class="weiteres">
						<h3 class="mb-2">Weiteres</h3>
	
						<?php if($config["showOther"] && $config["showEntryDate"]){ ?>
						<div class="row">
							<div class="col">
								<label for="eintritt">Eintrittsdatum</label>
								<input type="date" name="eintritt" placeholder="Eintrittsdatum" class="form-control" id="eintritt">
							</div>
						</div>
						<?php } ?>
						<?php if($config["showOther"] && $config["showNotesToClub"]){ ?>
						<div class="row">
							<div class="col">
								<label for="notiz">Notiz an den Verein</label>
								<textarea rows="4" name="notiz1" placeholder="Notiz" class="form-control" id="notiz1"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label>*Das Feld ist ein Pflichtfeld</label>
							</div>
						</div>
						<?php } ?>

   					<!--  Dies ist das eigentliche Notizfeld, das gesendet wird, es wird per Javascript aus den anderen gefüllt und ist normalerweise über die Bootstrap-Klasse d-none unsichtbar gemacht -->
						<textarea rows="4" name="notiz" placeholder="Notiz" class="form-control d-none" id="notiz"></textarea>







					</div>
					<div class="row">
							<div class="col-12 col-md-6 d-flex align-items-center py-3">
								<input value="Senden" type="submit" name="event" class="form-control btn btn-primary rounded-1 mr-2" id="submitBtn">
								<input value="Zurücksetzen" type="reset" name="event" class="form-control btn btn-secondary rounded-1" id="resetBtn">
							</div>
						</div>
						<div class="row">
							<div id="api_error_alert" class="mx-3 col col-md-6 alert alert-danger invisible">
								<p id="api_error_msg" class="mb-0"></p>
							</div>
						</div>
					<?php } ?>
	</form>
</div>
<script type="text/javascript">
         mgvo_verein = "VCBWB";
			let requiredFields = [];
			<?php 
				foreach($config["requiredFields"] as $key => $value){
					?>requiredFields.push(document.forms['mitedit'][<?php	echo "'$key'" ?>]);<?php
				}
			?>
		<?php
		   echo "const call_id ='".$this->call_id."';";
		   echo "const callidc ='".urlencode($callidc)."';";		
		
		   if(isset($formSettingsDecoded['addJS'])) {
		      require(dirname(__FILE__).'/'.$formSettingsDecoded['addJS']);
		   }
		   
		if($this->loadMitantragFiles){ // Ohne Wordpress (oder entsprechende Angabe beim Construct) werden die Files dirket geladen, sonst wird das über require in Wordpress erledigt.
			//require(dirname(__FILE__).'/mitantrag.js');
			require(dirname(__FILE__).'/mitantragJQ.js');	
		} ?>
</script>
