/**
 * Onchange need to be outside the jquery function
 */
/*
let selectedGroups = [];
function showGroupsAsChosen(input) {
	let groupName = input.nextSibling.innerHTML;
	let selectedGroupsSpan = document.getElementById('selectedGroups');

	if (selectedGroups.includes(groupName)) {
		let rIdx = selectedGroups.indexOf(groupName);
		selectedGroups.splice(rIdx, 1);
	} else {
		selectedGroups.push(groupName);
	}

	let groupsString = "";
	for (let selectedGroup of selectedGroups) {
		groupsString += selectedGroup + "<br>";
	}

	selectedGroupsSpan.innerHTML = groupsString;
}

function showCourseAsChosen() {
	let selectedGroupsSpan = document.getElementById('selectedGroups');
	let selectedCourse = document.querySelector('input[name="course"]:checked').nextSibling.innerHTML;
	selectedGroupsSpan.innerHTML = selectedCourse;
}

*/
//TODO: Refactor
/*
function searchGroups() {
	let groupSearch = document.getElementById('groupSearch');
	let filter = groupSearch.value.toUpperCase().replace(/\s/g, '');

	let groups = document.getElementsByClassName('dropdown-group-item');
	let txtValue = "";
	for (let i = 0; i < groups.length; i++) {
		txtValue = groups[i].children[1].innerHTML.replace(/\s/g, '');
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			groups[i].style.display = "";
		} else {
			groups[i].style.display = "none";
		}
	}
}

function searchCourses() {
	let courseSearch = document.getElementById('courseSearch');
	let filter = courseSearch.value.toUpperCase().replace(/\s/g, '');

	let courses = document.getElementsByClassName('dropdown-course-item');
	let txtValue = "";
	for (let i = 0; i < courses.length; i++) {
		txtValue = courses[i].innerHTML.replace(/\s/g, '');
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			courses[i].style.display = "";
		} else {
			courses[i].style.display = "none";
		}
	}
}

function changeTypeOfForm() {
	let selectGroupsSection = document.getElementById('selectGroupsSection');
	let selectCourseSection = document.getElementById('selectCourseSection');
	let showAsCourse = document.getElementById('showAsCourse');
	let inputPayment = document.getElementById('zahlweise');
	let reducedFee = document.getElementById('ermaessigung');
	let withoutWork = document.getElementById('ohneas');
	let selectedGroupsTitle = document.getElementById('selectedGroupsTitle');
	let selectedGroupsSpan = document.getElementById('selectedGroups');

	let groups = document.getElementsByClassName('dropdown-group-item');

	let showAsMgAntrag = inputPayment.classList.contains("invisible");
	if (showAsMgAntrag) {
		const courseChecked = document.querySelector('input[name="course"]:checked');
		if (courseChecked) {
			changeAttribute(courseChecked, "checked", false);
		}
		changeHTML(selectedGroupsSpan, "");
		addClassList(selectCourseSection, "d-none");
		removeClassList(selectGroupsSection, "d-none");
		changeHTML(showAsCourse, "Nur für einen Kurs anmelden");
		changeHTML(inputPayment.previousElementSibling, "Zahlweise");
		changeHTML(selectedGroupsTitle, "Ausgewählte Gruppen: ")
	} else {
		selectedGroups = [];
		for (let i = 0; i < groups.length; i++) {
			const group = groups[i].children[0];
			if (group.checked) {
				changeAttribute(group, "checked", false);
			}
		}
		changeHTML(selectedGroupsSpan, "")
		removeClassList(selectGroupsSection, "d-none");
		addClassList(selectCourseSection, "d-none");
		changeHTML(showAsCourse, "Doch für eine Gruppe anmelden");
		changeHTML(inputPayment.previousElementSibling, "Zahlweise = einmalig")
		changeHTML(selectedGroupsTitle, "Ausgewählter Kurs: ")
	}
	toggleClassList(inputPayment, "invisible");
	toggleClassList(reducedFee?.parentElement, "invisible");
	toggleClassList(withoutWork?.parentElement, "invisible");
}

function changeAttribute(node, attribute, value) {
	if (document.body.contains(node)) {
		node.setAttribute(attribute, value);
	}
}

function changeHTML(node, html) {
	if (document.body.contains(node)) {
		node.innerHTML = html;
	}
}

function addClassList(node, className) {
	if (document.body.contains(node)) {
		node.classList.add(className);
	}
}

function removeClassList(node, className) {
	if (document.body.contains(node)) {
		node.classList.remove(className);
	}
}

function toggleClassList(node, className) {
	if (document.body.contains(node)) {
		node.classList.toggle(className);
	}
} */