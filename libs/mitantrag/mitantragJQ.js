(function ($) {
	'use strict';
	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */ 
 
	$(document).ready(() => {
		
		AddStarToRequired();
		
		
		$('#mitedit').submit((e) => {
			e.preventDefault();
			if (isFilled()) {
				
				//addCourse();
				addToNotes();
				httpGet();
			} else {
				alert("Bitte füllen sie alle Pflichtfelder aus")
			}
			
		});
	});
    
    
	function AddStarToRequired() {
		//console.info("AddStarToRequired: requiredFields", requiredFields);
		let prevL;
		for (let inputField of requiredFields) {
			if (inputField.type === 'checkbox') {
			   $(inputField).css("background-color","#e83b4c"); // Sonderbehandlung für Checkboxen, dunkleres Rot
			} else {
			   $(inputField).css("background-color","#FDEAEA"); 
			}
			prevL = $(inputField).prevAll('label').first();
			//console.info("AddStarToRequired: Add Star to", prevL, " HTML:",$(prevL).html);
			$(prevL).html($(prevL).html()+"*");
		}
	}  
 

	function httpGet() {
		const url = "https://www.mgvo.de/prog/pub_createmit_post.php?call_id="+call_id+"&callidc="+callidc;
		let xhr = new XMLHttpRequest();
		xhr.open("POST", url, true);

		const form = $('#mitedit')[0];
		const formData = new FormData(form);
		xhr.send(formData);

		const entryContent = $('.entry-content');
		const mgAntrag = $('#mg-antrag');
		const errorAlert = $('#api_error_alert');
		const errorMsg = $('#api_error_msg');

		xhr.onreadystatechange = () => {
			if (xhr.readyState === 4) {
				let response = JSON.parse(xhr.responseText);
				if ((response.errno === -16) || (response.errno === -17)) { //-16 = mandatory field not delivered || -17 = field value out of range
					errorAlert.removeClass('invisible');
					errorMsg.html("Bitte fülle alle Felder aus");
				} else if (response.errno === 0) {
					errorAlert.addClass('invisible');
					mgAntrag.addClass('d-none');

					const successMsg = $('<p></p>');
					successMsg.html('Deine Anmeldung ist bei uns eingegangen!');
					successMsg.addClass('alert alert-success');
					entryContent.append(successMsg);
					if (mgvo_verein == "VCBWB") {
						window.location = "https://www.brandenburg-volleyball.de/mitglied-werden/abgesendet";
					}
				} else {
					errorAlert.removeClass('invisible');
					errorMsg.html('Leider ist ein Fehler aufgetreten. Bitte melde dich beim Vorstand des Vereins!');
				}
			}
		}
	}
	 


	function addGroups() {
		let numberOfGroups = 0;
		let groups = "";
		let gItems = document.getElementsByClassName('gItem');
		for (let g of gItems) {
			if (g.checked) {
				//	g.children[0].setAttribute("name", "gru0" + numberOfGroups);
				// 	g.children[0].setAttribute("value", g.children[0].id);
				groups = groups+" "+g.id;
				numberOfGroups++;
			}
		}
		if (numberOfGroups == 0) groups = "keine Auswahl!"
		return groups;
	}

	function addCourse() {
		let cItems = document.getElementsByClassName('cItem');
		for (let c of cItems) {
			if (c.checked) {
				c.setAttribute("name", "gru01");
				c.setAttribute("value", c.id);
			}
		}
	}


	/**
	 * requiredFields is being generated in the mitantrag.php file and is dependent on mitantragSettings.json file
	 * */


	function isFilled() {
		for (let inputField of requiredFields) {
			if (inputField.type === 'checkbox' && inputField.checked === true ) continue; // Checkbox und gesetzt
			inputField.classList.toggle("is-invalid", inputField.value == "")
		}
		for (let inputField of requiredFields) {
			if (inputField.type === 'checkbox' && inputField.checked === true ) continue;
			if (inputField.value == "") {
				return false;
			}
		}
		return true;
	} 

	function addToNotes() {
		let ermaessigung = document.getElementById("ermaessigung") != null ?  document.getElementById("ermaessigung").checked : false;
        let ohneas = document.getElementById("ohneas") != null ? document.getElementById("ohneas").checked : false;
		let notiz = document.getElementById("notiz");
		let notiz1 = document.getElementById("notiz1");
		let tarif = document.getElementsByName("tarif");
		switch (mgvo_verein) {
			case "BGC": 
					notiz.value = "Mit Ermaessigung: " + (ermaessigung ? "Ja" : "Nein") + "; Tarif: "+ tarif[0].value + "; Ohne Arbeitsstunden: " + (ohneas ? "Ja" : "Nein") + "; Gruppen: "+ addGroups() + " ;  " + notiz1.value ;
					breaks;
			case "VCBWB":
			 		notiz.value = notiz1.value ; break;
			default:
 					notiz.value = notiz1.value ;
		}
		//alert(notiz.value);
	} 

})(jQuery);