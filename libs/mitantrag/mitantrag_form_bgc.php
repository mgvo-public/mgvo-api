

		<style>
		<?php 
			if($this->loadMitantragFiles){ // CSS wird nur bei stand-alone-Nutuzung der sniplets eingebunden. Sonst wird dies im Wordpress-Plugin über WP-enquire vorgenommen.
				include(plugin_dir_path(__FILE__).'/mitantrag.css'); 
			}
		?>
		</style>
		<div class="container" id="mg-antrag">
			<?php if(false && $config["showGroups"] && $config["showCourses"]) { ?>
				<div class="alert alert-info" role="alert">
					<a onclick="changeTypeOfForm()" class="text-underlined" id="showAsCourse" role="button">Nur für einen Kurs anmelden</a>
				</div>
			<?php } ?>
			<form name="mitedit" id="mitedit" method="post" action="<?php echo $url ?>"  class="needs-validation" novalidate>
					<div class="anschrift">
						<h3>Anschrift</h3>
						<div class="row">
							<div class="col">
								<label for="anrede">Anrede</label> <br>
								<select name="anrede" class="form-control" id="anrede" required>
									<option value="" selected></option>
									<option value="1">Herr</option>
									<option value="2">Frau</option>
									<option value="3">Herr Dr.</option>
									<option value="4">Frau Dr.</option>
									<option value="5">Herr Prof. Dr.</option>
									<option value="6">Frau Prof. Dr.</option>
									<option value="7">Firma</option>
									<option value="8">Herr Mag.</option>
									<option value="9">Frau Mag.</option>
									<option value="10">Herr Mag. Dr.</option>
									<option value="11">Frau Mag. Dr.</option>
									<option value="12">Herr Mag. (FH)</option>
									<option value="13">Frau Mag. (FH)</option>
									<option value="14">Herr Ing.</option>
									<option value="15">Frau Ing.</option>
									<option value="16">Herr Dipl. Ing.</option>
									<option value="17">Frau Dipl. Ing.</option>
									<option value="18">Herr DDr.</option>
									<option value="19">Frau DDr.</option>
									<option value="20">keine Anrede</option>
									<option value="21">Herr und Frau</option>
									<option value="22">Familie</option>
									<option value="23">Herr Prof.</option>
									<option value="24">Frau Prof.</option>
								</select>
								<div class="invalid-feedback">
									Wie sollen wir Sie ansprechen?
								</div>
							</div>
							<div class="col">
								<label for="geschlecht">Geschlecht</label> <br>
								<select name="geschlecht" class="form-control" id="geschlecht" required>
									<option value="" selected></option>
									<option value="m">Männlich</option>
									<option value="w">Weiblich</option>
									<option value="s">sächlich (Firma)</option>
								</select>
								<div class="invalid-feedback">
									Bitte Feld ausfüllen.
								</div>
							</div>
						</div>
						<div class="row">            
							<div class="col">
								<label for="vorname">Vorname</label>
								<input type="text" class="form-control" placeholder="Vorname" name="vorname" id="vorname" required/>
								<div class="invalid-feedback">
									Bitte geben Sie Ihren Vornamen ein
								</div>
							</div>
							<div class="col">
								<label for="nachname">Nachname</label>
								<input type="text" class="form-control" placeholder="Nachname" name="nachname" id="nachname" required>
								<div class="invalid-feedback">
									Bitte geben Sie Ihren Nachnamen an.
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label for="gebdat">Geburtsdatum</label> <br>
								<input type="date" name="gebdat" class="form-control" id="gebdat" required/>
								<div class="invalid-feedback">
									Bitte geben Sie Ihr Geburtsdatum ein.
								</div>
							</div>
						</div>
						<?php if($config["showAddress"]){ ?>
							<div class="row">
								<div class="col">
									<label for="str">Straße</label> <br>
									<input type="text" name="str" placeholder="Straße" class="form-control" id="str" required/>
									<div class="invalid-feedback">
										Bitte geben Sie Ihre Straße an.
									</div>
								</div>
							</div>
							<div class="row">
									<div class="col">
										<label for="plz">Postleitzahl</label> <br>
										<input type="text" name="plz" placeholder="Postleitzahl" class="form-control" id="plz" required/>
										<div class="invalid-feedback">
											Bitte geben Sie Ihre Postleitzahl an.
										</div>
									</div>
									<div class="col">
										<label for="ort">Ort</label> <br>
										<input type="text" name="ort" placeholder="Ort" class="form-control" id="ort" required/>
										<div class="invalid-feedback">
											Bitte geben Sie Ihren Ort an.
										</div>
									</div>
							</div>
						<?php } ?>
						<?php if($config["showTel"]){ ?>
						<div class="row">
							<div class="col">
								<label for="tel1">Telefon</label>
								<input type="tel" name="tel1" placeholder="Telefon 1" class="form-control" id="tel1">
							</div>
							<?php if($config["showTel"] && $config["showSecondTel"]){ ?>
							<div class="col">
								<label for="tel2">Telefon 2</label>
								<input type="tel" name="tel2" placeholder="Telefon 2" class="form-control" id="tel2">
							</div>
							<?php } ?>
						</div>
						<?php } ?>
						<?php if($config["showMobile"]){ ?>
						<div class="row">
							<div class="col">
								<label for="mobil1">Mobiltelefon</label>
								<input type="tel" name="mobil1" placeholder="Mobiltelefon 1" class="form-control" id="mobil1">
							</div>
							<?php if($config["showMobile"] && $config["showSecondMobile"]){ ?>
							<div class="col">
								<label for="mobil2">Mobiltelefon 2</label>
								<input type="tel" name="mobil2" placeholder="Mobiltelefon 2" class="form-control" id="mobil2">
							</div>
							<?php } ?>
						</div>
						<?php } ?>
						<?php if($config["showEmail"]){ ?>
						<div class="row">
							<div class="col">
								<label for="email">E-Mail</label>
								<input type="email" name="email" placeholder="E-Mail" class="form-control" id="email" required>
								<div id="emailFeedback" class="invalid-feedback">
									Bitte geben Sie Ihre E-Mail an.
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if($config["showEmail"] && $config["showCommunication"]){ ?>
						<div class="row">
							<div class="col">
								<label for="zfld03">Versand Mitgliederinformation</label>
								<select name="zfld03" class="form-control" id="zfld03" required>
									<option value="email" selected>Digital per E-Mail</option>
									<option value="brief">Per Brief</option>
								</select>
								<small class="form-text text-muted">
									Auf diesem Wege senden wir Ihnen allgemeine Informationen zu.
								</small>
								<div class="invalid-feedback">
									Wie sollen wir Sie kontaktieren?
								</div>
							</div>
							<div class="col">
								<label for="zfld04">Versand rechtlich relevante Kommunikation</label>
								<select name="zfld04" class="form-control" id="zfld04" required>
									<option value="email" selected>Digital per E-Mail</option>
									<option value="brief">Per Brief</option>
								</select>
								<small class="form-text text-muted">
									Auf diesem Wege senden wir Ihnen alle rechtlichen relevanten Informationen zu (z.B. Einladung zur Mitgliederversammlung).
								</small>
								<div class="invalid-feedback">
									Wie sollen wir sie kontaktieren?
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
					<?php if($config["showPayment"]){ ?>
					<div class="zahlungsdaten">
						<h3>Zahlungsdaten</h3>
						<div class="row">
							<div class="col">
								<label for="kontoname">Zahler (Falls abweichend)</label>
								<input type="text" name="kontoname" placeholder="Zahler" class="form-control" id="kontoname">
							</div>
							<div class="col">
								<label for="blz">Bankleitzahl</label>
								<input type="text" name="blz" placeholder="Bankleitzahl" class="form-control" id="blz">
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label for="iban">IBAN</label>
								<input type="text" name="iban" placeholder="IBAN" class="form-control" id="iban">
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label for="bic">BIC</label>
								<input type="text" name="bic" placeholder="BIC" class="form-control" id="bic">
								<small class="form-text text-muted">
									Die BIC ben&ouml;tigen wir nur bei ausl&auml;ndischen Konten.
								</small>
							</div>
							<?php if($config["showPayment"] && $config["showFormOfPayment"]){ ?>
							<div class="col">
								<label for="zahlweise">Zahlweise</label>
								<select name="zahlweise" class="form-control" id="zahlweise" required>
									<option value="v" selected>vierteljährlich</option>
									<option value="h">halbjährlich</option>
									<option value="j">jährlich</option>
								</select>
								<div class="invalid-feedback">
									In welchen Intervallen sollen wir den Beitrag einziehen?
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="row d-none">
							<div class="col">
								<label for="zahlungsart">Zahlungsart</label>
								<select name="zahlungsart" class="form-control" id="zahlungsart">
									<option value="l" selected>Lastschrift</option>
								</select>
							</div>
						</div>
						<div class="row">
						<?php if($config["showPayment"] && $config["showReducedFee"]){ ?>
							<div class="col-12 col-md-6  d-flex py-3">
								<input class="col-2" type="checkbox" class="form-control" id="ermaessigung">
                        <label for="ermaessigung" class="col-10 mb-0 px-0">Erm&auml;&szlig;igter Beitrag (Student / in Ausbildung)</label>
								
							</div>
						<?php } ?>

						</div>
						<div class="row">
							<div class="col-12 col-md-6 d-flex py-3">
								<input class="col-2" type="checkbox" name="eev_conf" class="form-control" id="eev_conf" value="1">
								<label for="eev_conf" id="sepa-label" class="col-10 mb-0 px-0">SEPA-Mandatserteilung (Dieser muss zugestimmt werden)</label>
							</div>
							<p id="sepa-hint">Ich ermächtige den Blau-Gold Casino Darmstadt e.V., Zahlungen von meinem Konto mittels Lastschrift einzuziehen. Zugleich weise ich mein Kreditinstitut an, die von Blau-Gold Casino Darmstadt e.V. auf mein Konto gezogenen Lastschriften einzulösen. <br />
									Hinweis: Ich kann innerhalb von acht Wochen, beginnend mit dem Belastungsdatum, die Erstattung des belasteten Betrages verlangen. Es gelten dabei die mit meinem Kreditinstitut vereinbarten Bedingungen. <br />
									Gläubiger: Blau-Gold Casino Darmstadt e.V., Alsfelder Str. 45a, 64289 Darmstadt SEPA-Gläubiger-ID: DE52ZZZ00000856804 <br/>
							</p>
						</div>
					</div>
					<?php } ?>
					<?php if($config["showOther"]) { ?>
					<div class="weiteres">
						<h3 class="mb-2">Weiteres</h3>
						
						
						<?php if(true || $config["showOther"] && $config["showTarif"]){ ?>
						   <div class="col-12 d-flex py-3">
						       <p> Bitte wählen sie einen Tarif aus. Wenn sie sich nicht sicher sind, welchen sie benötigen, können sie auch einfach unten eine/mehrere Gruppen auswählen, wird ermittel diesen dann für Sie. Wenn sie ihre Gruppe hier nicht finden, prüfen sie auf der Webseite unter Angeboten ob dieses Angebot ein Kurs ist, der keine Mitgliedschaft erfordert (In der Beschreibung findet sich immer ein entsprechender Hinweis). Kurse können über diese Formular noch nicht gebucht werden. Die Kosten können Sie folgender Webseite entnehmen: <a href="https://blau-gold-darmstadt.de/verein/mitgliedsbeitrage/" target="_blank">Beitragsordnung</a> (Seite öffnet in neuen Tab/Fenster) </p>
						   </div>
						   <div class="row">
   							<div class="col-12 col-md-4  d-flex py-3">
   								<input class="col-2" type="radio" name="tarif" class="form-control" id="bs" value="BS">
   								<label for="bs" class=" mb-0 px-0">Breitensport</label>
   							</div>
   							<div class="col-12 col-md-4  d-flex py-3">
   							   <input class="col-2" type="radio" name="tarif" class="form-control" id="ts" value="TS">
   								<label for="ts" class="mb-0 px-0">Turniersport</label>
   							</div>
   							<div class="col-12 col-md-4  d-flex py-3">
   								<input class="col-2" type="radio" name="tarif" class="form-control" id="Forma" value="FORMA">
   								<label for="Forma" class=" mb-0 px-0">Standard-Formation</label>	
   							</div>
   							<div class="col-12 col-md-4  d-flex py-3">
   								<input class="col-2" type="radio" name="tarif" class="form-control" id="TM" value="TrainingsM">
   								<label for="TM" class=" mb-0 px-0">Trainingsmitgliedschaft</label>			
   							</div>
   							<div class="col-12 col-md-4  d-flex py-3">
   								<input class="col-2" type="radio" name="tarif" class="form-control" id="passiv" value="Passiv">
   								<label for="passiv" class=" mb-0 px-0">Passive Mitgliedschaft</label>
   							</div>
   							<div class="col-12 col-md-4  d-flex py-3">
   								<input class="col-2" type="radio" name="tarif" class="form-control" id="ermitteln" value="ermittlen" checked="checked">
   								<label for="ermitteln" class=" mb-0 px-0">Bitte Tarif ermitteln</label>
   							</div>
							</div>
						<?php } ?>	
						
						<?php if(true || $config["showOther"] && $config["showWithoutWorkingHours"]){ ?> 
							<div class="col"> Normalerweise muss jedes Mitglied ab 14 Jahren Arbeitsstunden im Verein leisten. Gegen einen erhöhten Beitrag können sich sich aber pauschal auch von diesen freistellen lassen.</div>
						
							<div class="col-12 col-md-4 d-flex py-3">
								<input class="col-2" type="checkbox" class="form-control" id="ohneas">
								<label for="ohneas" class="col-11 mb-0 px-0">Beitrag ohne Arbeitsstunden</label>	
							</div>
						<?php } ?>
					
						<?php if($config["showOther"] && $config["showGroups"]){ ?>		  
					      <div class="col-12 col-md-12 py-3">
					         <label>In welchen Gruppen möchten Sie teilnehmen <span id="tooManyGroupsNotice"> </span></label>		      
							<div id="groupaccordion" class="accordion mb-2 mt-2">
							
							<?php

							$resar = $this->sniplet_obj->api->read_gruppen();
							$groups = $this->sniplet_obj->mgvo_filter_gruppen($resar['objar'], ['kurs' => false, 'mitantrag' => true],[]) ; // Filter auf nur Gruppen (ohne Kursdatum) und nur mit mitantrag=1
							$filter = [ "kat" => "_GROUP" ];
							$grouped = $this->sniplet_obj->mgvo_group_gruppen($groups, $filter); 
							$i=1; 
							if (count($grouped) == 0 ) echo "Es konnte keine Gruppen gefunden werden";
							else {
   							//echo "<ul class='mgvo-gruppen-ul-li'>";							
   							foreach($grouped as $idx => $kat) {

   							   $grp_vorhanden = false;
   							   foreach($kat as $idx2 => $g) {
   							      if ($g['mgantrag'] == 1) $grp_vorhanden == true;
   							   }
   							   if (!$grp_vorhanden) continue; // Keine Gruppen in der Kategorie
   							   if ($idx == "Frühjahr2022") continue;
                           echo '<div class="card">';
                           echo '<div class="card-header" id="headingKat'.$i.'">';
                           echo '<h5 class="mb-0">';
                           echo '<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseKat'.$i.'" > 	&#149;&nbsp;';
                           echo $idx;
                           echo '</button> </h5> </div>';
                           echo '    <div id="collapseKat'.$i.'" class="collapse " aria-labelledby="headingOne" data-parent="#groupaccordion">';
                           echo '      <div class="card-body">';
                           echo "<ul>";
                           $first = true;
                           foreach($kat as $idx2 => $g) {
                              echo "<li>";
                              echo "<input type='checkbox' id=".$g['gruid']." class='gItem ";
                              if ($first){ $first = false; echo "show"; } // nur das erste Arccodion wird geöffnet
                              //echo " ' onclick='showGroupsAsChosen(this)'>";

                              echo " ' >";
                              echo "<label class='ml-1' for=".$g['gruid'].">".$g["grubez"];
                              echo "&nbsp;<br><span class='' style='font-size: 0.6px;'> (";
                              if (isset($g['gruzar'])) {
                                 foreach ($g['gruzar'] as $idz => $z) {
                                    echo isset($z['wotag']) ? $this->sniplet_obj->get_wotag($z['wotag'], true) : "";
                                    echo ", ";
                                    echo isset($z['startzeit']) ? substr($z['startzeit'], 0,5) :  "";
                                    echo "-";
                                    echo isset($z['endzeit']) ? substr($z['endzeit'], 0,5) :  "";
                                    echo "&nbsp;";
                                    echo $z['ortbez'] ?? $z['ortid'] ??  ""; //null-Koaleszenz-Operators
                                    echo "&nbsp; \n";
                                 }
                                 echo ")</span></label>";
                                 echo"</li>";
                              }
                           }  
                           echo "</ul>";
                           echo "</div> </div> </div> ";
                           $i++;
   							   
   							}
							}
                     ?>
                     
                     </div> 
                  </div> 
						
						<?php } ?>
						<?php if($config["showOther"] && $config["showCourses"]){ ?>
						<div class="row d-none" id="selectCourseSection">
							<div class="col">
								<label for="gruppe">Kurs</label> 
								<div class="dropdown d-flex">
									<input type="text" placeholder="suchen" id="courseSearch" class="input-group-text col" onkeyup="searchCourses()" />
									<button id="dropdown-toggle-courses" class="btn btn-secondary col dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
									</button>
									<div class="col course-dropdown dropdown-menu" style="max-height: 200px; overflow-y: auto;">
											<?php 
											foreach($groups as $g){
											   if(true ||  $g["grukat"] == "Kurs"){
													$dropdownCourseItem = "<input type='radio' id=".$g["gruid"]." class='cItem dropdown-course-item' name='course' onclick='showCourseAsChosen()'/>";
													$dropdownCourseItem .= "<label for=".$g["gruid"].">".$g["grubez"]."</label><br>";
													echo $dropdownCourseItem;
												}
											}
											?>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						
						<?php if(false && $config["showGroups"]){ // ACHTUNG, Deaktiviert
						   ?> 
						<div class="row">
							<div class="col">
								<span id="selectedGroupsTitle">Ausgewählte Gruppen:</span><br>
								<span id="selectedGroups"></span>
							</div>
						</div>
						<?php } ?>
						
						<?php if($config["showOther"] && $config["showEntryDate"]){ ?>
						<div class="row">
							<div class="col">
								<label for="eintritt">Eintrittsdatum, optional, sonst verwenden wir den nächsten 1. des Folgemonats</label>
								<input type="date" name="eintritt" placeholder="Eintrittsdatum" class="form-control" id="eintritt">
							</div>
						</div>
						<?php } ?>
						<?php if($config["showOther"] && $config["showNotesToClub"]){ ?>
						<div class="row">
							<div class="col">
								<label for="notiz">Notiz an den Verein</label>
								<textarea rows="4" name="notiz1" placeholder="Notiz" class="form-control" id="notiz1"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<label>*Das Feld ist ein Pflichtfeld</label>
							</div>
						</div>
						<?php } ?>

   					<!--  Dies ist das eigentliche Notizfeld, das gesendet wird, es wird per Javascript aus den anderen gefüllt und ist normalerweise über die Bootstrap-Klasse d-none unsichtbar gemacht -->
						<textarea rows="4" name="notiz" placeholder="Notiz" class="form-control d-none" id="notiz"></textarea>


					</div>
					
					Mit der Absendung erkenne ich die Satzung des TSV Blau-Gold Casino Darmstadt e.V. an und bin damit einverstanden, dass diese Daten elektronisch für vereinsinterne Zwecke, wie Abrechnungen und Mitteilungen, für die Dauer der Mitgliedschaft gespeichert und verwendet werden. <br />
						
					<div class="row">
							<div class="col-12 col-md-6 d-flex align-items-center py-3">
								<input value="Senden" type="submit" name="event" class="form-control btn btn-primary rounded-1 mr-2" id="submitBtn">
								<input value="Zurücksetzen" type="reset" name="event" class="form-control btn btn-secondary rounded-1" id="resetBtn">
							</div>
						</div>
						<div class="row">
							<div id="api_error_alert" class="mx-3 col col-md-6 alert alert-danger invisible">
								<p id="api_error_msg" class="mb-0"></p>
							</div>
						</div>
					<?php } ?>
	</form>
</div>
<script type="text/javascript">
         mgvo_verein = "BGC";
			let requiredFields = [];
			<?php 
				foreach($config["requiredFields"] as $key => $value){
					?>requiredFields.push(document.forms['mitedit'][<?php	echo "'$key'" ?>]);<?php
				}
			?>


		<?php 
		if(isset($formSettingsDecoded['addJS'])) {
		   require(dirname(__FILE__).'/'.$formSettingsDecoded['addJS']);
		}
		
		if($this->loadMitantragFiles){ // Ohne Wordpress (oder entsprechende Angabe beim Construct) werden die Files dirket geladen, sonst wird das über require in Wordpress erledigt.
			require(dirname(__FILE__).'/mitantrag.js');
			require(dirname(__FILE__).'/mitantragJQ.js');	
		} ?>
</script>
