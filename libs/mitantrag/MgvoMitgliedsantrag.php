<?php


class MgvoMitgliedsantrag {
   private  $sniplet_obj;
   private string $call_id;
   private string $vcryptkey;

   private string $mitantragConfig;
   private string $loadMitantragFiles;

   function __construct($sniplet_obj, $call_id,$vcryptkey ) {
      $this->sniplet_obj = $sniplet_obj;
      $this->call_id = $call_id;
      $this->vcryptkey = $vcryptkey;
   }
   
   /**
    * Die Funktion gibt den Mitgliedsantrag aus. Hierzu wird per default mitantrag_form.php geladen. 
    * $formname: Alternativer Formularname
    * $loadMitantragFiles: Wenn $loadMitantragFiles auf true gesetzt ist,werden auch die zugehörigen CSS-Files geladen.
    * $mitantragConfig: Konfiguration, was angezeigt werden soll.
    * 
    * Wenn call_id oder vcryptkey nicht gesetzt sind, wird ein Fehler ausgegeben. 
    *  
    */

   function mgvo_mitantrag($mitantragConfig = null, $loadMitantragFiles = true, $formname = '') {
      $html ="";
      if ($mitantragConfig != null) {
         $this->mitantragConfig = $mitantragConfig;
         $formSettings = $mitantragConfig;
         $formSettingsDecoded = json_decode($formSettings, true);
         $settings =$formSettingsDecoded;
         if ($formname == '' && isset($formSettingsDecoded['formname'])) {
            $formname = $formSettingsDecoded['formname'];
         } 
         $formSet= str_replace(".php", "", $formname);
         if ($formSet != '' && isset($formSettingsDecoded[$formSet])) {
            $settings = $formSettingsDecoded[$formSet]; // es werden unterschiedliche Settings je Formular akzeptiert

         }         
      }
      if ( str_contains($formname,"/")  or ! is_file(__DIR__."/".$formname)) { // Pfade sind im Formnamen nicht erlaubt => Sicherheit
         $html .= "Keine Pfade in der Angabe des Formnamens erlaubt: ".__DIR__."/".$formname;
         $formname = "mitantrag_form.php";
      } 
      $this->loadMitantragFiles = $loadMitantragFiles;
      
      // Die beiden Variabelen werden beim Construct aus der Wordpress-DB gefüllt. Diese müssen nat�rlich beide Admin-Dashbord unter Einstellungen / MGVO hinterlegt sein.
      $crypt = new Cipher($this->vcryptkey);
      $callidc = $crypt->encrypt($this->call_id);
      // die Call-ID wie auch die CallIDC m�ssen in die URL als Parameter �bergeben werden. (Die callidc kann aber auch in den Post-Daten stehen)
      $url = "https://www.mgvo.de/prog/pub_createmit_post.php?call_id=".$this->call_id."&callidc=".urlencode($callidc);

      $config = array();   
      $config["showAddress"] = $settings['showAddress']['visible'] ?? false;
      $config["showTel"] = $settings['showTel']['visible'] ?? false;
      $config["showSecondTel"] = $settings['showTel']['showSecondTel']['visible'] ?? false;
      $config["showMobile"] = $settings['showMobile']['visible'] ?? false; 
      $config["showSecondMobile"] = $settings['showMobile']['showSecondMobile']['visible'] ?? false;
      $config["showEmail"] = $settings['showEmail']['visible'] ?? false;
      $config["showCommunication"] = $settings['showEmail']['showCommunication']['visible'] ?? false;
      $config["showPayment"] = $settings['showPayment']['visible'] ?? false;
      $config["showFormOfPayment"] = $settings['showPayment']['formOfPayment']['visible'] ?? false;
      $config["showReducedFee"] = $settings['showPayment']['showReducedFee']['visible'] ?? false;
      $config["showWithoutWorkingHours"] = $settings['showOther']['showWithoutWorkingHours']['visible'] ?? false;
      $config["showOther"] = $settings['showOther']['visible'] ?? false;
      $config["showGroups"] = $settings['showOther']['showGroups']['visible'] ?? false;
      $config["showTarif"] = $settings['showOther']['showTarif']['visible'] ?? false;
      $config["showCourses"] = $settings['showOther']['showCourses']['visible']?? false;
      $config["showEntryDate"] = $settings['showOther']['showEntryDate']['visible']?? false;
      $config["showNotesToClub"] = $settings['showOther']['showNotesToClub']['visible']?? false;
      
      $config["showZusatzfelder"] = $settings['showZusatzfelder']['visible']?? false;
      for ($i = 1 ; $i<=16; $i++) {
         $zfdl_id = 'zfld'.( $i < 10 ? "0":"").$i;
         $config["zfld"][$i] = $settings['showZusatzfelder'][$zfdl_id]['visible'] ?? false;
      }
      $config["requiredFields"] = $settings['requiredFields']?? false;


      
      if ($this->call_id && $this->vcryptkey) {
         ob_start();
         require(__DIR__."/".$formname);
         $html .= ob_get_contents(); 
         ob_end_clean(); 

      } else {
         $html .= "Kein MGVO-Secret in den Einstellungen hinterlegt <br>";
      } 
      return $html;
      
   }
}
