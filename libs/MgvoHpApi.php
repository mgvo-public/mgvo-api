<?php
declare(strict_types=1);
require_once __DIR__.'/include/helper_functions.php';
require_once __DIR__.'/include/Cipher.php';


class MgvoHpApi {
    /** Available versions */
    protected array $versionar = ["2.0", "3.0"];
    /**
     * 0 = off
     * 1 = Debug (echo)
     * 2 = Debug (PHP Errorlog)
     * 4 = Debug (internal buffer)
     * multiple debug currently not supported
     */
    protected int $debuglevel = 0;
    public string $debugbuffer; // contain all debug messages, can be read and cleared whenever needed
    public ?array $debugfkt = null; // If not null, debug messages are only issued for the functions contained in the array
    protected string $call_id;
    protected string $vcryptkey;
    protected bool $cacheon = true;
    protected $cachetime;
    protected string $cachedir;
    protected string $version;
    protected array $tab; // Table after JSON conversion

    /**
     * @param string $call_id call_id of the club
     * @param string|null $vcryptkey Key for synchronous encryption. Is entered in MGVO in the technical parameters.
     * @param int $cachetime Sets the cache time in minutes. If not specified, 5 minutes will be set
     * @param string $cachedir
     * @param string $version
     */
    function __construct(
        string $call_id,
         $vcryptkey = null,
        int $cachetime = 5,
        string $cachedir = ".",
        string $version = ""
    ) {
        $this->call_id = $call_id;
        $this->vcryptkey = $vcryptkey;
        $this->cachetime = $cachetime * 60; // cachetime in seconds
        $this->cachedir = $cachedir;
        $this->set_version($version);
    }

    /**
     * Sets the version
     * @param string $version
     */
    function set_version(string $version) {
        if (empty($version) || !in_array($version, $this->versionar)) {
            //TODO print warning that version is not supported
            $this->version = $this->versionar[count($this->versionar) - 1];
        } else {
            $this->version = $version;
        }
    }

    /**
     * Reads all groups (Gruppen)
     */
    function read_gruppen(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(3);
        return $this->tab;
    }

    /**
     * Reads all group categories (Gruppenkategorien)
     */
    function read_grukat(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(4);
        return $this->tab;
    }

    /**
     * Reads all departments (Abteilungen)
     */
    function read_abt(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(5);
        return $this->tab;
    }

    /**
     * Reads all trainers/supervisors (Betreuer)
     */
    function read_betreuer(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(1);
        return $this->tab;
    }

    /**
     * Reads appointments of a calendar (Kalender)
     */
    function read_vkal(string $vkalnr, string $seljahr): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(30, ['vkalnr' => $vkalnr, 'seljahr' => $seljahr]);
        return $this->tab;
    }

    /**
     * Reads all locations (Orte)
     */
    function read_orte(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(2);
        return $this->tab;
    }

    /**
     * Reads all public events (Veranstaltungen)
     */
    function read_events(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(6);
        return $this->tab;
    }

    /**
     * Reads all training cancellations/failures (Trainingsausfälle)
     */
    function read_training_fail(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(31);
        return $this->tab;
    }

    /**
     * Reads all contribution groups (Beitragsgruppen)
     */
    function read_beigru(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(12);
        return $this->tab;
    }

    /**
     * Reads all rates (Tarife)
     */
    function read_tarife(): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(11);
        return $this->tab;
    }

    /**
     * eads all public documents, possibly only of a specific document type (Dokumente)
     */
    function read_documents(?string $dokart = null): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(30, ['dokart' => $dokart]);
        return $this->tab;
    }

    /**
     * Reads members based on selection parameters:
     * - General search term: suchbeg
     * - Age/Date of birth: suchalterv - suchalterb
     * - Resignation (Austritt): suchaustrittv - suchaustrittb
     * - Group ID: suchgruid
     * - Contribution group (Beitragsgruppe): suchbeigru
     * - Direct debit payer (Lastschriftzahler): lssel (Selection value: 1)
     * - Cash payer/Transfer (Barzahler/Überweiser): barsel (Selection value: 1)
     * - Standing order (Dauerauftrag): dasel (Selection value: 1)
     * - Gender: geschl (x,m,w)
     * - Member: ausgetr (x,m,a)
     * - Active/Passive: aktpass (x,a,p)
     * - Mail recipient (Mailempfänger): mailempf (x,e,s)
     * - Domestic/Foreign (Inland/Ausland): landsel (x,i,a)
     * - Dunning level (Mahnstufe): selmahnstufe (a,1,2,3)
     */
    function read_mitglieder(array $selparas): array {
        $this->cacheon = true;
        $this->tab = $this->call_request(7, $selparas, true);
        return $this->tab;
    }

    /**
     * Reads a member (Mitglied)
     */
    function show_mitglied(int $mgnr): array {
        $this->read_mitglieder(['suchbeg' => $mgnr]);
        return $this->tab['objar'][0];
    }

    /**
     * Reads the data of the member's passport photo
     */
    function get_mitpict(int $mgnr): array {
        $this->tab = $this->call_request(8, ['mgnr' => $mgnr], true);
        return $this->tab;
    }

    /**
     * Creates a member. The parameters correspond to the import data.
     * Return codes:
     * - Parameter missing = -1
     * - Mandatory field missing = -16
     * - Field value out of defined range = -17
     * - Assigned membership number (successful) = 0
     *
     * This function can also be used to update an existing member data record.
     * For this purpose, the membership number (mgnr) and all parameters to be changed must be provided.
     * @return array The table contains the return code and text
     */
    function create_mitstamm(array $inar): array {
        $this->tab = $this->call_request(20, ['inar' => $inar], true);
        return $this->tab;
    }


    /**
     * Updates a member. The parameters correspond to the import data.
     * Return codes:
     * - Parameter missing = -1
     * - Mandatory field missing = -16
     * - Field value out of defined range = -17
     * - Assigned membership number (successful) = 0
     *
     * The membership number (mgnr) and all parameters to be changed must be provided.
     * @return array The table contains the return code and text
     */
    function update_mitstamm(array $inar): array {
        return $this->create_mitstamm($inar);
    }

    /**
     * Logs in a member. The member number (mgnr) and the password (passwd) must be provided.
     * Return codes:
     * - Login ok = 1
     * - Password not ok / user not available = 0
     * - Max. logon attempts exceeded = 11
     * - Secret code generated and sent to mobile device, logon must be done with code = 12
     * - Secret code (SMS code) does not match = 13
     * @param string $email_id E-Mail
     * @param string $passwd Password
     * @param string $smscode SMS-Code
     * @return int Return code
     */
    function login(string $email_id, string $passwd, string $smscode): int {
        $this->cacheon = false;
        $paras = http_build_query([
            'call_id' => $this->call_id,
            'email_id' => $email_id,
            'passwd' => $passwd,
            'smscode' => $smscode
        ]);
        return (int) http_get("https://www.mgvo.de/prog/pub_mgb_validate.php?$paras");
    }

    /////////////////// Common functions ///////////////////

    function set_debuglevel($debuglevel) {
        global $mgvo_debug;
        $this->debuglevel = $debuglevel;
        $mgvo_debug = $debuglevel;
    }

    /**
     * Calls the api_entry.php program centrally for all requests.
     */
    function call_request(int $reqtype, array $paras = [], bool $secured = false, bool $postflg = false): array {
        $url = "https://www.mgvo.de/api/api_entry.php";
        $cparas = [
            'reqtype' => $reqtype,
            'outmode' => 1,              // JSON
            'call_id' => $this->call_id,
            'version' => $this->version,
        ];
        if ($secured) {
            $paras['call_id'] = $this->call_id;
            $paras['time'] = time();
            $parasc = paras_encrypt($paras, $this->vcryptkey);
            $cparas['paras'] = $parasc;
        } 
        else {
            $cparas = array_merges($cparas, $paras);
        }
        $urlparas = http_build_query($cparas);

        $filename = "$this->cachedir/mgvo-hpapi-$urlparas.cache";
        if ($this->cacheon && is_file($filename)) {
            $filetime = filemtime($filename);
            if (time() - $filetime <= $this->cachetime) {
               $cachecont = file_get_contents($filename);
               if (str_contains($cachecont, "Sicherheitsversto")) { // e.g. if API was not enabled in past
                  error_log("MGVO API Cache enthält ungültige Daten:".$filename);
                  mgvo_log("API Error: Cache enthält ungültige Daten:", $filename , MGVO_DEBUG_ERR);
               } else {
                  $ret = $cachecont;
               }
                
            }
        }
        if (empty($ret)) {
            if ($postflg) {
                $ret = http_post($url, $cparas);
            } 
            else {
                $ret = http_get("$url?$urlparas");
            }
            if (str_contains($ret, "Sicherheitsversto")) {
               error_log("MGVO API HTTP-Aufruf enthält Sicherheitsverstoß:"."$url?$urlparas");
               mgvo_log("API Error: HTTP-Aufruf  enthält Sicherheitsverstoß", "$url?$urlparas" , MGVO_DEBUG_ERR);
            }
            elseif (empty($ret) || substr($ret, 0, 5) == "ERROR") {
                mgvo_log("API Error:", substr($ret, 5), MGVO_DEBUG_ERR);
            }
            elseif ($this->cachetime > 0) {
                file_put_contents($filename, $ret);
            }
        }
        $ergtab = json_decode($ret, true);
        $objname = $ergtab['objname'] ?? [];
        if (isset($ergtab[$objname])) {
            $ergtab['objar'] = $ergtab[$objname];
        }
        unset($ergtab[$objname]);
        $this->api_debug(__FUNCTION__, "Ergebnisstabelle", $ergtab);
        return $ergtab ?? [];
    }

    function api_debug(string $funktion, string $text = "Enter Funktion", $val = '') {
         if ($this->debuglevel == 0 || $this->debugfkt && !in_array($funktion, $this->debugfkt)) {
            return;
        }
        switch ($this->debuglevel) {
            case 1:
                // Line breaks are converted to HTML (<br>), arrays are output as HTML structures
                echo "Debug API: $funktion : $text ".(!empty($val)?"=>":" "). nl2br($this->obsafe_print_r($val) . "\n");
                break;
            case 2:
                error_log("Debug API: $funktion : $text =>" . print_r($val, true) . "\n");
                break;
            case 4:
                // Line breaks are converted to HTML (<br>), arrays are output as HTML structures
                $this->debugbuffer .= "Debug API: $funktion : $text =>" . nl2br($this->obsafe_print_r($val) . "\n");
                break;
            case 0;
                // Debug off
                break;
        }
    }

    function api_printdebug() {
        echo $this->debugbuffer;
    }

    /**
     * Outputs the debug data to a hidden div.
     * This can either be made visible via the browser's developer mode
     * or required via a JS/JQuery script,
     * that the class .d-none { display: none;} is set (default for Bootstrap).
     */
    function api_printdebug_hidden() {
        if ($this->debuglevel != 4) {
            return;
        }
        // 'd-none mgvoapi-debug'
        echo "<div class= 'mgvoapi-debug'>";
        echo $this->debugbuffer;
        echo "</div>";
    }

    /**
     * Alternative print_r which returns HTML
     */
    function obsafe_print_r($var, $return = true, $html = true, $level = 0) {
        $spaces = "";
        $space = $html ? "&nbsp;" : " ";
        $newline = $html ? "<br />" : "\n";
        for ($i = 1; $i <= 6; $i++) {
            $spaces .= $space;
        }
        $tabs = $spaces;
        for ($i = 1; $i <= $level; $i++) {
            $tabs .= $spaces;
        }

        if (is_array($var)) {
            $title = "Array";
            $strukt = true;
        } elseif (is_object($var)) {
            $title = get_class($var) . " Object";
            $strukt = true;
        } else {
            return print_r($var, true);
        }
        $output = $title . $newline;
        $output .= "<div style='font-size:0.6rem'>";
        foreach ($var as $key => $value) {
            if (is_array($value) || is_object($value)) {
                $level++;
                $value = $this->obsafe_print_r($value, true, $html, $level);
                $level--;
            }
            $output .= $tabs . "[" . $key . "] => " . $value . $newline;
        }
        $output .= "</div>";
        if ($return) {
            return $output;
        }
        echo $output;
        return null;
    }
}
