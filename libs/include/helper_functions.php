<?php
declare(strict_types=1);

// Helper functions

// Values for mgvo_debug:
const MGVO_DEBUG_ERR = 1;         // General error output
const MGVO_DEBUG_ALL = 255;       // All

function mgvo_log($comment, $lv, $dtyp) {
    global $mgvo_debug;
    if (($dtyp & $mgvo_debug) > 0) {
        if (!empty($comment) && is_string($lv) && !empty($lv)) {
            $txt = "$comment: $lv";
            error_log($txt);
        } else {
            if (!empty($comment)) {
                error_log($comment);
            }
            if (!empty($lv)) {
                $ret = print_r($lv, true);
                error_log($ret);
            }
        }
    }
}

/**
 * Serializes the parameter array $paras, encrypts it and encodes the string as URL parameter according to RFC 3986
 */
function paras_encrypt(array $paras, string $cryptkey): string {
    $sparas = serialize($paras);
    $cipher = new Cipher($cryptkey);
    $sparasc = $cipher->encrypt($sparas);
    return rawurlencode($sparasc);
}

/**
 * Decodes the $parasc parameter and unserializes the parameter into a parameter array
 */
function paras_decrypt(string $parasc, string $cryptkey): array {
    $dparasc = rawurldecode($parasc);
    $cipher = new Cipher($cryptkey);
    $dparas = $cipher->decrypt($dparasc);
    return unserialize($dparas);
}

function localecho(string $ipadr, $out, int $t = 0) {
    if ($_ENV['REMOTE_ADDR'] == $ipadr) {
        if (is_array($out)) {
            print_ar($out);
        } else {
            echo $out . "<br>";
        }
    }
    flush();
    if ($t != 0) {
        sleep($t);
    }
}

function utf8_dec(string $str): string {
    return mb_convert_encoding($str, "CP1252", "UTF-8");
}

function saveassign(array $arr, $idx, $initval) {
    return $arr[$idx] ?? $initval;
}

function prep_ar(array $ar): array {
    foreach ($ar as $key => $value) {
        if (is_array($value)) {
            prep_ar($value);
        } else {
            $ar[$key] = htmlentities((string) $value);
        }
    }
    return $ar;
}

function print_ar(array $ar) {
    echo "<pre>";
    print_r(prep_ar($ar));
    echo "</pre>";
}

function http_get(string $url, string $auth = "", array $optar = []) {
    global $glob_debug, $glob_curlerror_no, $glob_curlerror_msg;
    if ($glob_debug) {
        echo "</center>";
        echo "URL: $url<br>";
        echo "Auth:$auth<br>";
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    if ($auth != "") {
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if (isset($optar['sslcheck_off']) && $optar['sslcheck_off']) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    }
    if ($glob_debug) {
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
    }
    $result = curl_exec($ch);
    $glob_curlerror_no = 0;
    $glob_curlerror_msg = "";
    if (curl_errno($ch)) {
        $glob_curlerror_no = curl_errno($ch);
        $glob_curlerror_msg = curl_error($ch);
        if ($glob_debug) {
            echo "FehlerNr.: $glob_curlerror_no Fehler: $glob_curlerror_msg<br>";
            echo "HTTP-Code: " . curl_getinfo($ch, CURLINFO_HTTP_CODE) . "<br>";
            echo "Lookup-Time: " . curl_getinfo($ch, CURLINFO_NAMELOOKUP_TIME) . "<br>";
            echo "Connect-Time: " . curl_getinfo($ch, CURLINFO_CONNECT_TIME) . "<br>";
            echo "Primary Port: " . curl_getinfo($ch, CURLINFO_PRIMARY_PORT) . "<br>";
            echo "Header Size: " . curl_getinfo($ch, CURLINFO_HEADER_SIZE) . "<br>";
            echo "SSL Verify Result: " . curl_getinfo($ch, CURLINFO_SSL_VERIFYRESULT) . "<br>";
        }
    }
    curl_close($ch);
    if ($glob_debug) {
        echo "Returnwert: $result<br><br>";
    }
    return $result;
}

function http_post(string $url, ?array $vars = null, string $auth = "", array $optar = []) {
    global $glob_curlerror_no, $glob_curlerror_msg, $glob_debug;
    if ($glob_debug) {
        echo "</center>";
        echo "URL: $url<br>";
        print_ar($vars);
        echo "Auth:$auth<br>";
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    if ($auth != "") {
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }
    if ($optar['headerflag']) {
        curl_setopt($ch, CURLOPT_HEADER, 1);
    } else {
        curl_setopt($ch, CURLOPT_HEADER, 0);
    }
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if ($optar['sslcheck_off']) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    }
    if ($vars != null) {
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($optar['uploadflag'] == 0) {
            $postvar = "";
            foreach ($vars as $fn => $fv) {
                $postvar .= $postvar == "" ? "" : "&";
                $postvar .= "$fn=" . urlencode($fv);
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvar);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
        }
    }
    $result = curl_exec($ch);
    $glob_curlerror_no = 0;
    $glob_curlerror_msg = "";
    if (curl_errno($ch)) {
        $glob_curlerror_no = curl_errno($ch);
        $glob_curlerror_msg = curl_error($ch);
    }
    curl_close($ch);
    if ($glob_debug) {
        echo "Returnwert: $result<br>";
    }
    return $result;
}

function date2user(string $db_datum, int $typ = 1): string {
    if ($db_datum == "" || $db_datum == "0000-00-00") {
        return "";
    }
    $year = substr($db_datum, 0, 4);
    $syear = substr($db_datum, 2, 2);
    $month = substr($db_datum, 5, 2);
    $day = substr($db_datum, 8, 2);
    $datum = "";
    switch ($typ) {
        case 1:
            $datum .= "$day.$month.$year";  // 03.07.2005
            break;
        case 3:
            $datum .= "$day.$month.$syear"; // 03.07.05
            break;
        case 4:
            $datum .= "$day.$month.";  // 03.07.
            break;
        case 5:
            $day = (int) $day;        // 3.7.
            $month = (int) $month;
            $datum .= "$day.$month.";
            break;
        case 6:
            $day = (int) $day;        // 3.7.2005
            $month = (int) $month;
            $datum .= "$day.$month.$year";
            break;
        case 7:
            $day = (int) $day;        // 3.7.05
            $month = (int) $month;
            $datum .= "$day.$month.$syear";
            break;
    }
    return $datum;
}

function time2user(string $time): string {
    if ($time == "") {
        return "";
    }
    $va = sscanf($time, "%2d:%2d:%2d");
    $usertime = sprintf("%d:%02d", $va[0], $va[1]);
    if ($va[2] != 0) {
        $usertime .= sprintf(":%02d", $va[2]);
    }
    return $usertime;
}

function emptyval($fval): bool {
    if (empty($fval) || $fval == "0000-00-00" || $fval == "00:00:00" || $fval == "00:00" ||
        $fval == "0000-00-00 00:00:00" || is_numeric($fval) && $fval == 0.0) {
        return true;
    }
    return false;
}

function utf8_enc(string $str): string {
    return mb_convert_encoding($str, "UTF-8", "CP1252");
}

/**
 * Accepts any number of arrays as parameters. The arrays are checked for
 *  existence and all real arrays are merged into one array with array_merge.
 */
function array_merges(array ...$args): array {
    $idxar = [];
    foreach ($args as $idx => $mar) {
        if (is_array($mar)) {
            $idxar[] = $idx;
        }
    }
    if (count($idxar) == 1) {
        return $args[$idxar[0]];
    }
    $targetar = [];
    foreach ($idxar as $idx) {
        $targetar = array_merge($targetar, $args[$idx]);
    }
    return $targetar;
}

function check_callid(string $callid): bool {
    return strlen($callid) >= 32;
}
