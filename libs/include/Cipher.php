<?php
declare(strict_types=1);

class Cipher {
    const method = "AES-256-CBC";

    private string $sslkey;
    private string $iv;
    private int $ivlen;

    function __construct(string $textkey) {
        if ($textkey != "") {
            $this->init($textkey);
        }
    }

    function init(string $textkey) {
        $this->ivlen = openssl_cipher_iv_length(Cipher::method);
        $str = openssl_random_pseudo_bytes($this->ivlen);
        $this->iv = substr(hash('sha256', $str), 0, $this->ivlen);
        $this->sslkey = hash('sha256', $textkey);
    }

    function encrypt(string $input, bool $nobase64 = false): string {
        $encrypted = openssl_encrypt($input, Cipher::method, $this->sslkey, 0, $this->iv);
        $encrypted = $this->iv . $encrypted;
        return $nobase64 ? $encrypted : base64_encode($encrypted);
    }

    function decrypt(string $input, bool $binarymode = false, bool $nobase64 = false): string {
        $input = $nobase64 ? $input : base64_decode($input);
        $iv = substr($input, 0, $this->ivlen);
        $input = substr($input, $this->ivlen);
        $decrypted = openssl_decrypt($input, Cipher::method, $this->sslkey, 0, $iv);
        return $binarymode ? $decrypted : rtrim($decrypted, "\0");
    }
}
