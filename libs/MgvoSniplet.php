<?php

// In der Datei wird eine Sniplet-Klasse definiert, welche die Methoden der Klasse MGVO_HPAPI (ext_mod_hp.php)
// aufruft und zu den jeweiligen Daten HTML-Code zur Ausgabe der Daten generiert.
// Die Klasse repräsentiert Beispielcode und muss den individuellen Anforderungen angepasst werden.

class MgvoSniplet {
    public MgvoHpApi $api;
    private string $headline;
    public int $debuglevel; // 0 = off, 1= Debug (echo), 2 = Debug (PHP Errorlog), 4 = Debug (internal buffer), multiple debug currently not supported

    private string $call_id;
    private string $vcryptkey;

    //private $mitantragConfig;
    //private $loadMitantragFiles;
    private $verein;

    /**
     * dies ist die Funktionsendung für vereinsspezifische Erweiterungen, kann geändert werden
     * @param string $call_id call_id des Vereins
     * @param string $vcryptkey Schlüssel für die synchrone Verschlüsselung. Wird in MGVO in den technischen Parametern eingetragen
     * @param int $cachemin Anzahl Minuten, die ein Ergebnis von MGVO in einem Dateicache zwischengespeichert wird.
     *                  Wenn zu viele Ausrufe MGVO erreichen, wird aus Sicherheitsgründen kein Ergebnis mehr geliefert.
     */
    function __construct(string $call_id,  $vcryptkey = "", int $cachemin = 5) {
       $this->api = new MgvoHpApi($call_id, $vcryptkey, $cachemin);

        $this->call_id = $call_id;
        $this->vcryptkey = $vcryptkey;

        $this->set_debuglevel(0);
        $this->api->debugfkt = [
            'mgvo_filter_gruppen',
           'mgvo_sniplet_gruppen'
        ]; //Debug nur für die aufgelisteten Funktion, wenn leer, werden alle Funktionen angezeigt
    }

    function set_debuglevel(int $debuglevel = 0) {
        $this->api->set_debuglevel($debuglevel); // Debuglevel for API
        $this->debuglevel = $debuglevel;
    }

    function set_headline(string $headline) {
        $this->headline = $headline;
    }

    function write_headline(string $mgvo_headline = ""): string {
        return "<h2>" . (empty($this->headline) ? $mgvo_headline : $this->headline) . "</h2>";
    }

    function mgvo_sniplet_vkal(string $vkalnr, string $seljahr): string {
        // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
        $resar = $this->api->read_vkal($vkalnr, $seljahr);
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Termine";

        $sniplet = "<div class='mgvo mgvo-vkal'>" . ($this->write_headline($resar['headline'])) .
                    "<table class='mgvo-table-b1p1s0'><tr>
                        <th>Bezeichnung</th>
                        <th>Startdatum</th>
                        <th>Startzeit</th>
                        <th>Enddatum</th>
                        <th>Endzeit</th>
                        <th>Ort</th></tr>";
        
        
        foreach ($resar['objar'] as $vkr) {
            $sniplet .= sprintf("<tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                </tr>",
                $vkr['bez'] ?? "",
                date2user($vkr['startdat'] ?? ""),
                $vkr['startzeit'] ?? "",
                date2user($vkr['enddat'] ?? ""),
                $vkr['endzeit'] ?? "",
                $vkr['ort'] ?? ""
            );
        }
        $sniplet .= "</table></div>";
        return $sniplet;
    }

    function mgvo_sniplet_orte(array $a = []): string {
        // Liest die Ortsliste ein
        $resar = $this->api->read_orte();
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Orte gefunden";
        $sniplet = "<div class='mgvo mgvo-orte'>" . $this->write_headline($resar['headline']) .
                    "<table class='mgvo-orte'>
                        <tr>
                        <th>Orts-ID</th>
                        <th>Ortsbezeichnung</th>";
        if (!empty($a['link'])) {
            $sniplet .= "<th>Link Gruppen</th>";
        }
        $sniplet .= "</tr>";
        foreach ($resar['objar'] as $or) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$or[ortid]</td>";
            $sniplet .= "<td>$or[ortbez]</td>";
            if (!empty($a['link'])) {
                $sniplet .= "<td><a href='" . $a['link'] . "?ortid=" . $or['ortid'] . "'>Gruppen in $or[ortbez]</a></td>";
            }
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";
        return $sniplet;
    }

    // Liest die Betreuer ein und gibt sie als Tabelle aus
    function mgvo_sniplet_betreuer(): string {
        $resar = $this->api->read_betreuer();
        if (count($resar['objar']) == 0 ) return "Keine Betreuer gefunden";
        $sniplet = "<div class='mgvo mgvo-betreuer'>";
        $sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<table class='mgvo-betreuer'>";
        $sniplet .= "<tr>";
        //$sniplet .= "<th>Trainer-ID</th>";
        $sniplet .= "<th>Name</th>";
        $sniplet .= "<th>Telefon</th>";
        $sniplet .= "</tr>";
        foreach ($resar['objar'] as $or) {
            $sniplet .= "<tr>";
            //$sniplet .= "<td>$or[trid]</td>";
            $sniplet .= "<td>$or[nachname], $or[vorname]</td>";
            $sniplet .= "<td>";
            $sniplet .= isset($or['tel1']) ? $or['tel1'] : "";
            $sniplet .= "</td>";
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";
        return $sniplet;
    }

   /**
    * Gibt eine Tabelle mit den Betreuern und zugehöhrigen Gruppen aus
    *
    * @param string $entry
    * @return string
    */
    public function mgvo_sniplet_instructor_by_group($entry) {
        $groups = $this->api->read_gruppen();
        if (!isset($groups['objar']) or  count($groups['objar']) == 0 ) return "Keine Orte gefunden";
        $instructorlist = array();

        // baut eine liste mit den Trainen auf sowie ein Array mit Trainername als Key und allen Gruppen dazu
        foreach ($groups['objar'] as $idx => $group) {
            if (!isset($group['trnameall'])) {
                continue;
            } // Gruppe ohne Trainer
            $groupinstructors = explode(";", $group['trnameall']); // Gruppen können mehrere Trainer haben
            foreach ($groupinstructors as $instructor) {
                if ($instructor == "") {
                    continue;
                }
                $instructors[] = trim($instructor); // Baue Liste der Trainer auf
                if (array_key_exists(
                    trim($instructor),
                    $instructorlist
                )) { // baue eine Liste mit Trainern auf, die für jeden Trainer die Liste der GruppenIDs enthält
                    $instructorlist[trim($instructor)] = $instructorlist[trim(
                            $instructor
                        )] . " " . $idx; // An Liste für diesen Trainer anhängen
                } else {
                    $instructorlist[trim($instructor)] = $idx;
                }
            }
        }
        $instructors2 = array_unique($instructors);
        asort($instructors2);

        $o = "<table class='well table table-sm'>";
        $o .= "<thead><tr><th>Trainer</th><th>Gruppen</th></tr></thead>";
        $o .= "<tbody>";
        foreach ($instructors2 as $instructor) {
            $o .= "<tr><td>{$instructor}</td>";
            $grouplist = explode(" ", $instructorlist[$instructor]);
            $o .= "<td>";
            if (count($grouplist) > 0) {
                foreach ($grouplist as $id) {
                    $o .= "<a href='" . $entry . "/?index=" . $id . "&generic=yes'>" . $groups['objar'][$id]['grubez'] . "</a></br>";
                }
            }
            $o .= "</td>";
            $o .= "</tr>";
        }
        $o .= "</tbody>";
        $o .= "</table>";
        return $o;
    }

    /**
     * Gibt eine Liste der Veranstaltungen aus.
     * Mit dem Paramter "kurz"="ja" (oder jeder Zeichenkette) wird eine verkürzte Form z.B. für Sidebars ausgegeben
     *
     * @param array $filter
     */
    function mgvo_sniplet_events($filter = null) {
        // Liest die öffentlichen Veranstaltungen
        $resar = $this->api->read_events();
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Termine gefunden";
        $sniplet = "<div class='mgvo mgvo-events'>";
        $sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<table class='mgvo-events'>";
        $sniplet .= "<tr>";

        if (isset($filter['kurz']) && !empty($filter['kurz'])) {
            $sniplet .= "<th>Event/Ort</th>";
            $sniplet .= "<th>Datum/Zeit</th>";
         } else {
            $sniplet .= "<th>Event</th>";
            $sniplet .= "<th>Beschreibung</th>";
            $sniplet .= "<th>Ort</th>";
            $sniplet .= "<th>Datum</th>";
            $sniplet .= "<th>Zeit</th>";
            $sniplet .= "<th>Bestell-URL</th>";
        }
        $sniplet .= "</tr>";
        foreach ($resar['objar'] as $or) {
            $sniplet .= "<tr>";

            if (isset($filter['kurz']) && !empty($filter['kurz'])) {
                $sniplet .= "<td>$or[name] ";
                $sniplet .= isset($or['ort']) ? "$or[ort]</td>" : "</td>";
                $sniplet .= isset($or['startdate']) ? "<td>" . date2user($or['startdate'], 1) . " " : "<td>";
                $sniplet .= isset($or['starttime']) ? "$or[starttime]</td>" : "</td>";
            } else {
                $sniplet .= "<td>$or[name]</td>";
                $sniplet .= isset($or['description']) ? "<td>$or[description]</td>" : "<td></td>";
                $sniplet .= isset($or['ort']) ? "<td>$or[ort]</td>" : "<td></td>";
                $sniplet .= isset($or['startdate']) ? "<td>" . date2user($or['startdate'], 1) . "</td>" : "<td></td>";
                $sniplet .= isset($or['starttime']) ? "<td>$or[starttime]</td>" : "<td></td>";
                if (!empty($or['besturl'])) {
                    $sniplet .= "<td><a href='https://www.mgvo.de/$or[besturl]' target=_blank>Bestell-Link</a></td>";
                } else {
                    $sniplet .= "<td></td>";
                }
            }
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";
        return $sniplet;
    }

    /**
     * Gibt eine Tabelle mit den Gruppen zurück
     *
     * @param ?array $filter Filtern, wie sie z.B. von Wordpress Shortcodes als $attr geliefert werden. Siehe mgvo_filter_gruppen()
     * @param string $gruppenlink
     */
    function mgvo_sniplet_gruppen(?array $filter = null, ?string $gruppenlink = null): string {
        $resar = $this->api->read_gruppen();
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Gruppen gefunden";
        $ar = $this->mgvo_filter_gruppen($resar['objar'], $filter);
        $sniplet = "<div class='mgvo mgvo-gruppen'>";
        $sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<table class='mgvo-gruppen'>";
        $sniplet .= "<tr>";
        //$sniplet .= "<th>Gruppen-ID</th>";
        $sniplet .= "<th>Name</th>";
        $sniplet .= "<th>Betreuer</th>";
        $sniplet .= "</tr>";
        $this->api->api_debug(__FUNCTION__, "Gruppenlink: " . $gruppenlink);
        foreach ($ar as $or) {
            $sniplet .= "<tr>";
            //$sniplet .= "<td>".$or['gruid']."</td>";
            if (!empty($gruppenlink)) {
                $url = $gruppenlink . "/?gruid=" . $or['gruid'];
                $sniplet .= "<td><a href='" . $url . "'>" . $or['grubez'] . "</a></td>";
            } else {
                $sniplet .= "<td>" . $or['grubez'] . "</td>";
            }
            $sniplet .= "<td>";
            $sniplet .= isset($or['trnameall']) ? $or['trnameall'] : "";
            $sniplet .= "</td>";
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";
        $this->api->api_printdebug_hidden();
        return $sniplet;
    }

    /**
     * Identisch zu mgvo_sniplet_gruppen, gibt die Gruppen aber als ul_li liste zurück.
     *
     * @param array|string $filter Filtern, wie sie z.B. von Wordpress Shortcodes als $attr geliefert werden. Siehe mgvo_filter_gruppen()
     * @param string $gruppenlink
     */
    function mgvo_sniplet_gruppen_ul_li($filter = "", ?string $gruppenlink = null): string {
        $this->api->api_debug(__FUNCTION__);
        $resar = $this->api->read_gruppen();
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Gruppen gefunden";
        $ar = $this->mgvo_filter_gruppen($resar['objar'], $filter);
        $this->api->api_debug(__FUNCTION__, "Array nach Filterung:", $ar);
        $grouped = $this->mgvo_group_gruppen($ar, $filter);
        $this->api->api_debug(__FUNCTION__, "Anzahl einträge in \$grouped " . count($grouped));
        $sniplet = "<div class='mgvo mgvo-gruppen-ul-li'>";
        //$sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<ul class='mgvo-gruppen-ul-li'>";
        if (count($grouped) == 0) { // keine Gruppierung vorgenommen
            $this->api->api_debug(__FUNCTION__, "Ausgabe ohne Gruppierung ");
            foreach ($ar as $idx => $or) {
                $sniplet .= "<li>";
                $sniplet .= "<span>" . $or['grubez'] . "</span>";
                $sniplet .= "</li>";
            }
         } else { // aktuelle nur 2 Ebenen unterstützt
            $this->api->api_debug(__FUNCTION__, "Ausgabe mit Gruppierung ");
            foreach ($grouped as $idx => $ar) {
                $sniplet .= "<li>";
                $sniplet .= "<span>" . $idx . "</span>";
                $sniplet .= "<ul class='mgvo-gruppen-ul-li-grouped'>";
                //$this->api->api_debug(__FUNCTION__, "Ausgabe einer Gruppeliste",$ar);
                foreach ($ar as $idx2 => $or) {
                    //$this->api->api_debug(__FUNCTION__, "Ausgabe eines Gruppe",$or);
                    $sniplet .= "<li>";

                    if (!empty($gruppenlink)) {
                        $url = $gruppenlink . "/?gruid=" . $or['gruid'];
                        $sniplet .= "<span><a href='" . $url . "'>" . $or['grubez'] . "</a></span>";
                  } else {
                        $sniplet .= "<span>" . $or['grubez'] . "</span>";
                    }

                    $sniplet .= "</li>";
                }
                $sniplet .= "</ul>";
                $sniplet .= "</li>";
            }
        }
        $sniplet .= "</ul>";
        $sniplet .= "</div>";
        //$this->api->api_printdebug_hidden();
        return $sniplet;
    }

    /**
     * Gibt eine einzelne Gruppe als Tabelle aus
     *
     * @param int $arrayindex
     *           (aus readgruppen())
     * @param string $gruid
     */
    function mgvo_sniplet_gruppen_entry(string $arrayindex = '', string $gruid = ''): string {
        $this->api->api_debug(__FUNCTION__);
        $resar = $this->api->read_gruppen();
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Gruppen gefunden";
        
        $ar = $this->mgvo_filter_gruppen($resar['objar'], []
        ); // Wichtig, hier werden nicht für die Webseite bestimmte Teile gefiltert.
        $sniplet = "<div class='mgvo mgvo-gruppe'>";
        $found = false;
        foreach ($ar as $idx => $or) {
            $this->api->api_debug(
                __FUNCTION__,
                "idx:" . $idx . " arrayindex:" . $arrayindex . " gruid:" . $gruid . " or(gruid):" . $or['gruid']
            );
            if (($arrayindex != '' && $idx == $arrayindex) || $gruid == $or['gruid']) {
                $this->api->api_debug(__FUNCTION__, "entry", $or);
                $sniplet .= "<table class='mgvo-gruppe'>";
                $sniplet .= "<tr><th>Gruppenname</th>";
                $sniplet .= "<td>" . $or['grubez'] . "</td></tr>";
                $sniplet .= "<tr><th>Beschreibung</th>";
                $sniplet .= "<td>";
                $sniplet .= isset($or['grutxt']) ? $or['grutxt'] : "";
                $sniplet .= "</td></tr>";
                // $sniplet .= "<tr><th>Kategorie / Verband</th>";
                // $sniplet .= "<td>";
                // $sniplet .= isset($or['grukat']) ?  $or['grukat'] : "";
                $sniplet .= "</td></tr>";
                $sniplet .= "<tr><th>Abteilung</th>";
                $sniplet .= "<td>";
                $sniplet .= isset($or['abtid']) ? $or['abtid']  :  "";
                $sniplet .= "</td></tr>";

                $sniplet .= "<tr><th>Alterstufe</th>";
                $sniplet .= "<td>";
                $sniplet .= isset($or['hp_altersstufe']) ? $or['hp_altersstufe'] : "";
                $sniplet .= "</td></tr>";
                $sniplet .= "<tr><th>Leistungsstufe</th>";

                $sniplet .= "<td>";
                $sniplet .= isset($or['hp_leiststufe']) ? $or['hp_leiststufe'] : "";
                $sniplet .= "</td></tr>";
                $sniplet .= "<tr><th>Alterstufe</th>";

                $sniplet .= "<td>";
                $sniplet .= isset($or['hp_kat']) ? $or['hp_kat'] : "";
                $sniplet .= "</td></tr>";

                $sniplet .= "<tr><th>Betreuer</th>";
                $sniplet .= "<td>";
                $sniplet .= isset($or['trnameall']) ? $or['trnameall'] : "";
                $sniplet .= "</td></th>";

                // Die Gruppenzeiten sind in einm Array abgelegt, da eine Gruppe mehrere Zeiten haben kann, offen: Termine aus Gruppen-Terminkalender
                foreach ($or['gruzar'] as $idz => $z) {
                    $sniplet .= "<tr><th>Zeit / Ort</th>";
                    $sniplet .= "<td>";
                    $sniplet .= isset($z['wotag']) ? $this->get_wotag($z['wotag']) : "";
                    $sniplet .= ", ";
                    $sniplet .= isset($z['startzeit']) ? substr($z['startzeit'], 0, 5) : "";
                    $sniplet .= "&nbsp;-&nbsp;";
                    $sniplet .= isset($z['endzeit']) ? substr($z['endzeit'], 0, 5) : "";
                    $sniplet .= "&nbsp;<span class='mgvo-ort'> ";
                    $sniplet .= isset($z['ortbez']) ? $z['ortbez'] : (isset($z['ortid']) ? $z['ortid'] : "");
                    $sniplet .= "</span></td></th>";
                }
                $sniplet .= "</table><br/>";
                $found = true;
            }
        }
        if (!$found) {
            $sniplet .= "Die Gruppe mit Index: " . $arrayindex . " oder GruppenID: " . $gruid . " konnte leider nicht gefunden werden<br/>";
        }
        $sniplet .= "</div>";
        $this->api->api_printdebug_hidden();
        return $sniplet;
    }

    /**
     *
     * Gruppiert Gruppen mit den Parametern aus der URL oder z.B. shortcodes aus Wordpress
     * $ar: das array, so wie es von read_gruppen() geliefert wird
     * $a: Argumente für die Gruppen als Array, wie sie z.B. von Wordpress Shortcodes als $attr geliefert werden. Eine Vorbehandlung mit shortcode_atts() ist sinnvoll, diese belegt die ungenutzten auch mit "" vor.
     * $get: Gruppenargumente aus der URL. Wenn diese direkt $_GET übernommen werden sollen, ist null zu übergeben, wenn sie nichte berücksichtigt werden sollen, dann []
     * Bei gleichen Filterangaben hat die im shortcode ($a) vorrang und überschreibt die Werte der URL
     * Gruppen mit hp_disp = 2 (nicht auf der Homepage anzeigen) werden automatisch immer ausgefiltert
     *
     * Die Gruppierung kann mehrfach vorgenommen werden (_GROUP1, _GROUP2, _GROUP3)
     *
     * Mögliche Gruppierungen:
     *    altersstufe, leiststufe, kat, grukat
     *
     *    Achtung:
     *    In MGVO müssen mehrere Werte mit ";" getrennt werden.
     *    Im Shortcode müssen mehrere Werte mit " " (Blank) getrennt werden.
     *    In der URL müssen mehrere Werte mit "+" getrennt werden, was von urldecode zu " " umgewandelt wird.
     *    Mehrere Gruppen und Filterwerte werden mit "oder" behandelt. D.h. in MGVO als Werte "Kinder;Jugendliche" und im Shortcode "Jugendliche Senioren" matched, d.h. die Gruppe wird zur Anzeige zurückgegegeben.
     *    Wenn in MGVO keine Werte eingetragen ist, aber ein Filterwert übergeben wird, wird der Gruppeneintrag nicht übernommen/angezeigt.
     *
     *    Wenn der Parameter $get nicht angebenen wird, werden auch die URL-Paramter ausgewertet. Ansonsten können hier alternative Fitlerwerte angegeben werden.
     *    Wenn die URL-Paramter nicht ausgewertet werden sollen, ist $get = [] zu übergeben.
     *
     *    D.h. es ist möglich mit der Filterfunktion und der Grupierungsfunktion gleichzeit zu arbeiten:
     *    $a = [ "altersstufe" => "Erwachsene Senioren _GROUP", 'leiststufe' = "Anfänger" ]
     *    gibt alle Gruppen aus, die Angebote für Erwachsene + Senioren enthalten und gruppierte diese nach Altersstufe.
     *    Bei mehrfachen Angaben von _GROUP etc. wird nur die erste Angabe berücksichtigt. Mehrfaches verschachtels Gruppieren wird (noch) nicht unterstützt.
     *
     * @param array $ar:
     *           array with groups
     * @param array $a:
     *           parameters as array
     * @param ?array $get:
     *           optional parameter from URL, wenn nicht vorhanden/null werden die URL-Parameter selbsttätig ausgewertet. Wenn leeres Array, werden eine URL-Auswertung vorgenommen.
     */
    function mgvo_group_gruppen(array $ar, array $a, ?array $get = null): array {
        $this->api->api_debug(__FUNCTION__);

        if ($get === null) {
            $get = $_GET;
        } // holen der Parameter aus der URL

        $para_array = [
            'hp_altersstufe' => 'altersstufe',
            'hp_leiststufe' => 'leiststufe',
            'hp_kat' => 'kat',
            'grukat' => 'grukat',
            'ortid' => 'ortid',
            'abtid'=>'abt'
        ];

        $para_text = [
            'hp_altersstufe' => 'Ohne Altersstufe',
            'hp_leiststufe' => 'Keine Leistungsstufe',
            'hp_kat' => 'Keine Kategorie',
            'grukat' => 'Keine Gruppenkategorie',
            'ortid' =>'Ohne Ortsangabe',
            'abtid'=>'Keiner Abteilung zugeordnet'
        ];
        foreach ($para_array as $para) {
            if (empty($a[$para])) { // Parameter aus dem shortcode nicht gesetzt?
                if (isset($get[$para])) { // Parameter in der URL gesetzt?
                    $a[$para] = $get[$para]; // Übernehme den Parameter
                }
            }
            $paralist = isset($a[$para]) ? explode(" ", $a[$para]) : [];
         if (array_search("_GROUP",$paralist,true) !== false) $a['group'][$para] = true;
        }
        $this->api->api_debug(__FUNCTION__, "Filter Array nach URL-Auswertung", $a);
        $this->api->api_debug(__FUNCTION__, "Array vor Gruppierung:", $ar);
        $grouped = [];
        foreach ($para_array as $idx => $para) {
            if (isset($a['group'][$para])) {
                foreach ($ar as $entry) {
                    if (!empty($entry[$idx])) {
                     $kats = explode(",",$entry[$idx]); // Wenn dem Parameter mehrere Werte sind, duplizierte den Eintrag
                     foreach ($kats as $kat) {
                        $kat2 = trim($kat);
                        if (!empty($kat2)) { // Sicherstellen, das wirklich ein Wert enthalten ist
                           $grouped[$kat2][] = $entry;
                           $this->api->api_debug("__FUNCTION__","group:", $kat2);
                           $this->api->api_debug("__FUNCTION__", "entry:", $entry);
                           $this->api->api_debug("__FUNCTION__", "grouped:", $grouped);
                        }
                     }
                  } else {

                        $grouped[$para_text[$idx]][] = $entry;
                        $this->api->api_debug("__FUNCTION__", "group: ohne_" . $para);
                        $this->api->api_debug("__FUNCTION__", "entry:", $entry);
                        $this->api->api_debug("__FUNCTION__", "grouped:", $grouped);
                    }
                }
                $this->api->api_debug(__FUNCTION__, "Gruppierung vorgenommen für:", $para);
                $this->api->api_debug(__FUNCTION__, "Ergebnis:", $grouped);
                break; // nur eine Gruppierung unterstützt
            }
        }
        return $grouped;
    }

    /**
     * Filter Gruppen mit den Parametern aus der URL oder z.B.
     * shortcodes aus Wordpress
     * $ar: das array, so wie es von read_gruppen() geliefert wird
     * $a: Filterargumente als Array, wie sie z.B. von Wordpress Shortcodes als $attr geliefert werden. Eine Vorbehandlung mit shortcode_atts() ist sinnvoll, diese belegt die ungenutzten auch mit "" vor.
     * $get: Filterargumente aus der URL. Wenn diese direkt $_GET übernommen werden sollen, ist null zu übergeben, wenn sie nichte berücksichtigt werden sollen, dann []
     * Bei gleichen Filterangaben hat die im shortcode ($a) vorrang und überschreibt die Werte der URL
     * Gruppen mit hp_disp = 2 (nicht auf der Homepage anzeigen) werden automatisch immer ausgefiltert
     *
     * Wenn der Parameter $get nicht angebenen wird, werden auch die URL-Paramter ausgewertet. Ansonsten können hier alternative Fitlerwerte angegeben werden.
     * Wenn die URL-Paramter nicht ausgewertet werden sollen, ist $get = [] zu übergeben.
     *
     * Filter:
     *    altersstufe: filtert auf das Feld hp_altersstufe in MGVO
     *    leiststufe: filtert auf das Feld hp_leiststufe in MGVO
     *    kat: filtert auf das Feld hp_kat in MGVO
     *    grukat: Gruppenkategorie aus MGVO
     *    kurs: true/false Filter, ob die Gruppe ein Kurs (true) oder kein Kurs (false) ist. Dabei wir auf einen Eintrag im Feld "kursvon" geschaut.
     *    mitantrag: true Filter, ob bei der Gruppe das Flag mgantrag gesetzt ist. Überscheibt die Filterung nach "hp_disp" (Anzeigen Homepage), die sonst immer vorgenommen wird.
     *
     *    In MGVO müssen mehrere Werte mit ";" getrennt werden.
     *    Im Shortcode müssen mehrere Werte mit " " (Blank) getrennt werden.
     *    In der URL müssen mehrere Werte mit "+" getrennt werden, was von urldecode zu " " umgewandelt wird.
     *    Mehrere Filterwerte werden mit "oder" behandelt. D.h. in MGVO als Werte "Kinder;Jugendliche" und im Shortcode "Jugendliche Senioren" matched, d.h. die Gruppe wird zur Anzeige zurückgegegeben.
     *    Wenn in MGVO keine Werte eingetragen ist, aber ein Filterwert übergeben wird, wird der Gruppeneintrag nicht übernommen.
     *
     * @param array $ar:
     *           array with groups
     * @param array $a:
     *           parameters as array
     * @param array $get:
     *           optional parameter from URL, wenn nicht vorhanden/null werden die URL-Parameter selbsttätig ausgewertet. Wenn leeres Array, werden eine URL-Auswertung vorgenommen.
     */
    function mgvo_filter_gruppen($ar, $a, $get = null) {
        $this->api->api_debug(__FUNCTION__);
        if ($get === null) {
            $get = $_GET;
        }
        if ($a == null) {
            $a = [];
        }
        // Array mit allen technischen Namen auf MGVO sowie der zugehörigen Filterparameter
        $para_array = [
            'hp_altersstufe' => 'altersstufe',
            'hp_leiststufe' => 'leiststufe',
            'abtid' => 'abt',
            'hp_kat' => 'kat',
            'grukat' => 'grukat',
            'ortid' => 'ortid'
        ];

        $this->api->api_debug(__FUNCTION__, "Filter Array vor URL-Auswertung", $a);
        foreach ($para_array as $para) { // Übernehmen der Parameter aus der URL
            if (empty($a[$para])) { // Parameter aus dem shortcode
                if (isset($get[$para])) {
                    $a[$para] = $get[$para];
                }
            }
            $a['para'][$para] = isset($a[$para]) ? explode(" ", strtolower($a[$para])) : [];
            $this->api->api_debug(__FUNCTION__, "Filter für $para aus ".(isset($a[$para]) ?$a[$para] : "[nicht def.]" )." übernommen" , $a['para'][$para] );

            $key = array_search("_group", $a['para'][$para], false); // alle _GROUP ignorieren (nach strtolower)
            if ($key !== false) {
                unset($a['para'][$para][$key]); // _GROUP aus dem Array löschen
                $this->api->api_debug(__FUNCTION__, "_GROUP aus Filter gelöscht", $a);
            }
        }
        $this->api->api_debug(__FUNCTION__, "Filter Array nach URL-Auswertung", $a);
        $new_ar = array();
        foreach ($ar as $idx => $entry) {
            $this->api->api_debug(__FUNCTION__, "loop entry:", $entry);
            if (isset ($entry['hp_disp']) && $entry['hp_disp'] == 2 && !isset($a['mitantrag'])) {
                continue;
            } // Gruppe in MGVO von Anzeige ausgeschlossen (Ausname Mitgliedsantrag)
            if (isset($a['kurs'])) { // Filterung nach Kursen
                if ($entry['kursvon'] != "0000-00-00") { // isset( $entry['kursvon']) && !empty($entry['kursvon']) &&
                    $this->api->api_debug(__FUNCTION__, "Kein Kurs", $entry['gruid']);
               if (!$a['kurs']) continue (1); // wenn kein Kurs, aber Kurs angefordert
            } else {
                    $this->api->api_debug(__FUNCTION__, "Ist Kurs", $entry['gruid']);
                    if ($a['kurs']) {
                        continue(1);
                    } // wenn Kurs aber nur "keine Kurse" angefordert
                }
            }
            if (isset($a['mitantrag']) && $a['mitantrag']) {
                if (!isset($entry['mgantrag']) || !empty($entry['mgantrag']) || $entry['mgantrag'] == "1") {
                    // Soll in den Mitgliedsantrag
                    $this->api->api_debug(__FUNCTION__, "Soll in den Mitantrag", $entry['gruid']);
               } else {
                    $this->api->api_debug(__FUNCTION__, "Soll nicht in den Mitantrag", $entry['gruid']);
                    continue(1); // soll nicht im Mitgliedsantrag angezeigt werden
                }
            }

            foreach ($para_array as $idx => $para) {
               if ($idx == "ortid") {
                  $entry_val = [];
                  if (isset($entry['gruzar']) &&  count($entry['gruzar']) > 0) {
                     foreach ($entry['gruzar'] as $gruzar_entry) {
                        if (isset($gruzar_entry[$idx])) {
                              $entry_val[] = $gruzar_entry[$idx];
                        }
                     }
                  }
               } else {
                    $entry_val = isset($entry[$idx]) ? explode(";", $entry[$idx]) : [];
               }
               if ($entry_val == [] ) { 
                  $this->api->api_debug(__FUNCTION__, "Kein Wert zum Schlüssel $para gefunden");
               }
                  
               if (isset($a['para'][$para]) && count($a['para'][$para]) > 0 && !empty($a['para'][$para][0]) && !$this->match_ar($entry_val,$a['para'][$para])) {
                    $this->api->api_debug(__FUNCTION__, "entry: ".print_r($entry_val,true)."match not " . $para." in array", print_r( $a['para'][$para], true));
                    continue(2);
                }
            }

            $entry['grubez'] = $this->sanitize_group(
                $entry['grubez']
            );  // Herausfiltern von unerwünschten Teilen. Aktuell wird alles nach einem ! nicht angezeigt.
            $new_ar[] = $entry;
        }
        return $new_ar;
    }

    function match_ar($e, $f) {
       $this->api->api_debug(__FUNCTION__);
       $this->api->api_debug(__FUNCTION__, "Parameter: E: ".print_r($e,true)." F: ".print_r( $f, true) );

       foreach ($e as $en) {
          $enl = trim(strtolower($en));
          if (in_array($enl, $f)) { // Wert kommt im Filter vor
             $this->api->api_debug(__FUNCTION__, "Stimmt überein");
             return true;
          }
       }
       $this->api->api_debug(__FUNCTION__, "Stimmt nicht überein");
       return false;
    }

   /**
     * Erstellt eine Wochenübersicht, wenn ein Link angegeben wird, wird dieser mit onclick für die jeweilige Gruppe verknüpft.
     *
     * @param array $a
     * @param string $gruppenlink
     * @return string
     */
    public function mgvo_sniplet_group_cards(array $a, $gruppenlink =  null) {
        $this->api->api_debug(__FUNCTION__);
        $group_events = "";
        $source_groups = $this->api->read_gruppen();
        $this->api->api_debug(__FUNCTION__, "Filter", $a);

        $groups = $this->mgvo_filter_gruppen($source_groups["objar"], $a);

        // Wenn eine Gruppe mehrere Zeiten hat, wird der Gruppeneintrag vervielfältigt
         $addgroups = [];
        foreach ($groups as $idx => $group) {
            if (empty($group['gruzar'][0]['wotag'])) { // Keine Zeitangabe gefunden, dann löschen
                unset($groups[$idx]);
                continue;
            }
            $zidx = 0;
            do {
               if ($zidx == 0) {
                  $groups[$idx]['wotag'] = isset($group['gruzar'][$zidx]['wotag']) ? $group['gruzar'][0]['wotag'] : "";
            $groups[$idx]['ortid'] = isset($group['gruzar'][$zidx]['ortid']) ? $group['gruzar'][0]['ortid'] : "";
            $groups[$idx]['startzeit'] = isset($group['gruzar'][$zidx]['startzeit']) ? $group['gruzar'][0]['startzeit'] : "";
                  $groups[$idx]['endzeit'] = isset($group['gruzar'][$zidx]['endzeit']) ? $group['gruzar'][0]['endzeit'] : "";
                  $groups[$idx]['turnus'] = isset($group['gruzar'][$zidx]['turnus']) ? $group['gruzar'][0]['turnus'] : "";
            $groups[$idx]['ortbez'] = isset($group['gruzar'][$zidx]['ortbez']) ? $group['gruzar'][0]['ortbez']: "";
            } else {
               $addgroups[$idx]['gruid'] = $groups[$idx]['gruid'];
               $addgroups[$idx]['grubez'] = $groups[$idx]['grubez'];
               $addgroups[$idx]['wotag'] = isset($group['gruzar'][$zidx]['wotag']) ? $group['gruzar'][$zidx]['wotag'] : "";
               $addgroups[$idx]['ortid'] = isset($group['gruzar'][$zidx]['ortid']) ? $group['gruzar'][$zidx]['ortid'] : "";
               $addgroups[$idx]['startzeit'] = isset($group['gruzar'][$zidx]['startzeit']) ? $group['gruzar'][$zidx]['startzeit'] : "";
               $addgroups[$idx]['endzeit'] = isset($group['gruzar'][$zidx]['endzeit']) ? $group['gruzar'][$zidx]['endzeit'] : "";
               $addgroups[$idx]['turnus'] = isset($group['gruzar'][$zidx]['turnus']) ? $group['gruzar'][$zidx]['turnus'] : "";
               $addgroups[$idx]['ortbez'] = isset($group['gruzar'][$zidx]['ortbez']) ? $group['gruzar'][$zidx]['ortbez'] : "";}
       			$this->api->api_debug(__FUNCTION__, "Fix Time", $zidx);
            $this->api->api_debug(__FUNCTION__, "fix zeit", $group['gruzar'][0]);
        $zidx += 1;
				} while (isset($group['gruzar'][$zidx]['wotag']) && $zidx < 10) ; // solange es einen weiteren Zeiteintrag gibt
         }
         $groups = array_merge($groups, $addgroups); // führe duplizierte Gruppen hinzu

        $this->array_sort_by_column_custom_order($groups, 'wotag', '');

        $groups_per_day = [];
        foreach ($groups as $idx => $group) {
            $wo_tag = $group['wotag'];
            if (!array_key_exists($wo_tag, $groups_per_day)) {
                $groups_per_day[$wo_tag] = [];
            }
            $group['idx'] = $idx;
            $groups_per_day[$wo_tag][] = $group;
        }

        if (!empty($a['img'])) {
            // Hintergrundgrafiken funktiont optisch aber meißt nicht gut
            $html = "<div class='calendar-card' style=\"background: rgb(0, 38, 71)  url('{$a['img']}');\">";
         } else {
            $html = "<div class='calendar-card'>";
        }
        $first = true;
        foreach ($groups_per_day as $groups) {
           $neuer_tag = true;
           $this->array_sort_by_column($groups,'startzeit');
            if (count($groups) == 0) {
                continue;
            } // Keine Gruppen an diesem Tag

            foreach ($groups as $group) {
                if (!$this->filter_groups($a, $group)) {
                    continue;
                }
                if ($neuer_tag) {
                    $wo_tag = $group['wotag'];

                    if (!$first) {
                        $html .= '</div>'; /*close div from  cal-day container from last day*/
                    }
                    $first = false;
                    $html .= "<div class='cal-day'>";
                    $html .= "  <div class='card title'>";
                    $html .= "        <div class='card-header'>";
                    $html .= $this->get_wotag($wo_tag);
                    $html .= "        </div>";
                    $html .= "   </div>";
                    $neuer_tag = false;
                }
                if (!empty($gruppenlink)) {
                    $url = $gruppenlink . "?gruid=" . $group['gruid'];
                    $html .= '<div class="card content" onclick="window.location=\'' . $url . '\'";>';
               } else {
                    $html .= "<div class='card content'>";
                }

                $html .= "    <div class='card-header'>" . $this->sanitize_group($group['grubez']) . "</div>";
                $html .= "    <div class='card-body'>";
                $html .= "        <p>";
                //if (isset($group['gzfld02'])) { $html .= $group['gzfld02']; }
                $html .= "    <br/>";
                $html .= substr($group['startzeit'], 0, 5);
                $html .= '-';
                $html .= substr($group['endzeit'], 0, 5);
                $html .= ' Uhr';
                $html .= "    <br/>";
                if ($group['ortbez'] != "") {
                    $html .= $group['ortbez'];
               } else {
                    if ($group['ortkb'] != "") {
                        $html .= $group['ortkb'];
                  } else {
                        $html .= $group['ortid'];
                    }
                }
                $html .= "</p>";
                $html .= "    </div>";
                $html .= "</div>";
            }
        }
        //close div from cal-day container from last day
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function array_sort_by_column_custom_order(
        &$arr,
        $col,
        $custom_order,
        $dir = SORT_ASC
    ) { // Funktion nicht mehr variabel, da in Klasse verschoben.
        $sort_col = [];
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];// $this->get_index_days($row[$col]);
        }
        array_multisort($sort_col, $dir, $arr);
    }

    public function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = [];
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }
        array_multisort($sort_col, $dir, $arr);
    }


    public function get_wotag(string $val, bool $kurz = false): string {
        if ($kurz) {
            switch ($val) {
                case "1":
                    return "Mo.";
                case "2":
                    return "Di.";
                case "3":
                    return "Mi.";
                case "4":
                    return "Do.";
                case "5":
                    return "Fr.";
                case "6":
                    return "Sa.";
                case "7":
                    return "So.";
                default:
                    return "ERROR: Übergültiger Wochentag $val";
            }
         } else {
            switch ($val) {
                case "1":
                    return "Montags";
                case "2":
                    return "Dienstags";
                case "3":
                    return "Mittwochs";
                case "4":
                    return "Donnerstags";
                case "5":
                    return "Freitags";
                case "6":
                    return "Samstags";
                case "7":
                    return "Sonntags";
                default:
                    return "ERROR: Übergültiger Wochentag $val";
            }
        }
    }

   /* BGC Spezifisch, kann dann raus */
    public function filter_groups($a, $group) {
      if (!isset($group['gzfld03'])) $group['gzfld03'] = "";

      if (isset($a['category']) && !$this->multiple_match($group['gzfld03'],$a['category'])) { // wenn kein Feld in MGVO gefüllt oder nicht gefunden
            return false;
        }
        if (isset($a['agelevel']) && $a['agelevel'] != "" and isset($group['gzfld02'])) {
            if (!$this->multiple_match($group['gzfld02'], $a['agelevel'])) {
                return false;
            }
        }
        if (isset($a['level']) && $a['level'] != "" and isset($group['gzfld04'])) {
            if (!$this->multiple_match($group['gzfld04'], $a['level'])) {
                return false;
            }
        }
        if (isset($a['gruid']) && $a['gruid'] != "" and isset($group['gruid'])) {
            if (!$this->multiple_match($group['gruid'], $a['gruid'])) {
                return false;
            }
        }
        return true;
    }

    /* BGC spezifisch, kann dann raus */
    public function multiple_match($list1, $list2): bool {
        if ($list2 != "" and isset($list1)) {
            $ar1 = explode(",", $list1);
            $ar2 = explode(",", $list2);
            //error_log ("Kategorie MGVO".print_r($mgvo_groups, true));
            //error_log ("Kategorie wp plugin".print_r($categories, true));
            if (count($ar1) > 0 && count($ar2) > 0) {
                foreach ($ar2 as $i => $c) {
                    $ar2[$i] = trim($ar2[$i]);
                }
                $found = false;
                foreach ($ar1 as $ar1_entry) {
                    if (in_array(trim($ar1_entry), $ar2)) {
                        $found = true;
                        return true;
                    }
                }
                if ($found == false) {
                    return false;
                }
            } else {
                // in einem der beiden Felder war nicht gültige Einträge, trotzdem weiter
            }
         }  else {
        }
        return true;
    }

   /* BGC Spezifisch, kann dann raus */
    public function sanitize_group($groupname) {
        // alles nach "!" entfernen
        if (stristr($groupname, "!")) {
            $groupname = stristr($groupname, "!", true);
        }
        return strtr($groupname, ["'" => ""]);
    }

    function mgvo_sniplet_abteilungen($a = []): string {
        $resar = $this->api->read_abt();
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Abteilungen gefunden";
        
        $sniplet = "<div class='mgvo mgvo-abteilungen'>";
        $sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<table class='mgvo-abteilungen'>";
        $sniplet .= "<tr>";
        $sniplet .= "<th>Abteilungs-ID</th>";
        $sniplet .= "<th>Name</th>";
        if (!empty($a['link'])) {
            $sniplet .= "<th>Link Gruppen</th>";
        }
        $sniplet .= "</tr>";
        foreach ($resar['objar'] as $or) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$or[abtid]</td>";
            $sniplet .= "<td>$or[abtbez]</td>";
            if (!empty($a['link'])) {
                $sniplet .= "<td><a href='" . $a['link'] . "?abt=" . $or['abtid'] . "'>Gruppen in $or[abtbez]</a></td>";
            }
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";

        return $sniplet;
    }

    function mgvo_sniplet_training_fail(): string {
        $resar = $this->api->read_training_fail();
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Kein Trainingsausfall";
        

        $sniplet = "<div class='mgvo mgvo-trainingfail'>";
        $sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<table class='mgvo-trainingsausfall'>";
        $sniplet .= "<tr>";
        $sniplet .= "<th>Gruppe / Belegung</th>";
        $sniplet .= "<th>Datum</th>";
        $sniplet .= "<th>Zeit</th>";
        $sniplet .= "<th>Ort</th>";
        $sniplet .= "<th colspan=3>neu</th>";
        $sniplet .= "<th>Grund / Veranstaltung</th>";
        $sniplet .= "</tr>";
        foreach ($resar['objar'] as $tfr) {
            $sniplet .= "<tr>";
            if (!empty($tfr['grbez'])) {
                $sniplet .= "<td>" . $tfr['grbez'] . " (" . $tfr['gruid'] . ")</td>";
            } else {
                $sniplet .= "<td>" . $tfr['belbez'] . "</td>";
            }
            $sniplet .= "<td>" . date2user($tfr['sdat'], 1) . "</td>";
            $sniplet .= "<td>" . time2user($tfr['starttime']) . " - " . time2user($tfr['endtime']) . "</td>";
            $sniplet .= "<td>" . $tfr['ortsbez'] . "</td>";
            if (!empty($tfr['neudat'])) {
                $sniplet .= "<td>" . date2user($tfr['neudat'], 1) . "</td>";
            } else {
                $sniplet .= "<td></td>";
            }
            if (!empty($tfr['neuzeithtml'])) {
                $sniplet .= "<td>" . $tfr['neuzeithtml'] . "</td>";
            } else {
                $sniplet .= "<td></td>";
            }
            if (!empty($tfr['neuorthtml'])) {
                $sniplet .= "<td>" . $tfr['neuorthtml'] . "</td>";
            } else {
                $sniplet .= "<td></td>";
            }
            $sniplet .= "<td>" . $tfr['ebez'] . "</td>";
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";
        return $sniplet;
    }

    function mgvo_sniplet_read_mitglieder($selparar = null): string {
        // Selektion von Mitgliedern.
        $resar = $this->api->read_mitglieder($selparar);
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Mitglieder gefunden";
        
        $sniplet = "<div class='mgvo mgvo-mitglieder'>";
        $sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<table class='mgvo-mitglieder'>";
        $sniplet .= "<tr>";
        $sniplet .= "<th>MgNr.</th>";
        $sniplet .= "<th>Nachname</th>";
        $sniplet .= "<th>Vorname</th>";
        $sniplet .= "<th>Stra&szlig;e</th>";
        $sniplet .= "<th>PLZ</th>";
        $sniplet .= "<th>Ort</th>";
        $sniplet .= "<th>Eintritt</th>";
        $sniplet .= "<th>Austritt</th>";
        $sniplet .= "</tr>";
        foreach ($resar['objar'] as $mr) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$mr[mgnr]</td>";
            $sniplet .= "<td>$mr[nachname]</td>";
            $sniplet .= "<td>$mr[vorname]</td>";
            $sniplet .= "<td>$mr[str]</td>";
            $sniplet .= "<td>$mr[plz]</td>";
            $sniplet .= "<td>$mr[ort]</td>";
            $sniplet .= "<td>" . date2user($mr['eintritt'], 1) . "</td>";
            $sniplet .= "<td>" . date2user($mr['austritt'], 1) . "</td>";
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";
        return $sniplet;
    }

    function mgvo_sniplet_show_mitglied($mgnr): string {
        $mr = $this->api->show_mitglied($mgnr);
        $sniplet = "<div class='mgvo mgvo-mitglieder'>";
        $sniplet .= $this->write_headline("Anzeige Mitglied");
        $sniplet .= "<table class='mgvo-mitglied'>";
        foreach ($mr as $fieldname => $value) {
            if(is_array($value)){
                $value = json_encode($value);
            }
            $sniplet .= "<tr><td>$fieldname:</td><td>$value</td></tr>";
        }
        $sniplet .= "</table>";
        $sniplet .= "</div>";
        return $sniplet;
    }

    function mgvo_sniplet_list_documents($dokart = null): string {
        $resar = $this->api->list_documents($dokart);
        if (!isset($resar['objar']) or  count($resar['objar']) == 0 ) return "Keine Dokumente gefunden";
        
        $sniplet = "<div class='mgvo mgvo-documents'>";
        $sniplet .= $this->write_headline($resar['headline']);
        $sniplet .= "<table class='mgvo-dokulist'>";
        $sniplet .= "<tr>";
        $sniplet .= "<th>DokNr.</th>";
        $sniplet .= "<th>Dokart</th>";
        $sniplet .= "<th>Name</th>";
        $sniplet .= "<th>Gr&ouml;&szlig;e</th>";
        $sniplet .= "</tr>";
        foreach ($resar['objar'] as $dokr) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>" . $dokr['doknr'] . "</td>";
            $sniplet .= "<td>${dokr['dokart']}</td>";
            $sniplet .= "<td><a href='" . $dokr['url_display'] . "' target=_blank>{$dokr['dokbez']}</a></td>";
            $sniplet .= "<td>" . $dokr['fsize'] . "</td>";
            $sniplet .= "</tr>";
        }
        $sniplet .= "</table><br>";
        $sniplet .= "</div>";
        return $sniplet;
    }

    function mgvo_sniplet_mitpict($mgnr) {
        $resar = $this->api->get_mitpict($mgnr);
        if($resar == []) {
            echo "Kein Bild vorhanden";
            return;
        }

        $mpr = $resar['objar'][0];
        $dokname = $mpr['dokname'];
        $ctype = $mpr['mimetype'];
        $content = base64_decode($mpr['content']);

        header("Content-Type: $ctype");
        header("Content-Length: " . strlen($content));
        header("Content-disposition: inline; filename=\"$dokname\"");

        echo $content;
    }

    function mgvo_sniplet_selbstauskunft_iframe(): string {
        if (!check_callid($this->call_id)) {
            return "Keine MGVO Call_ID angegeben, bitte unter Settings eintragen";
        }
        return "<div><iframe src='https://www.mgvo.de/prog/pub_ssrequest.php?call_id=" . $this->call_id . "' width='500' height='400px' frameborder='0'>Selbstauskunft kann nicht eingebunden werden</iframe></div><br>";
    }

    function mgvo_sniplet_mitgliedsantrag_link(): string {
        if (!check_callid($this->call_id)) {
            return "Keine MGVO CallID angegeben, bitte unter Settings eintragen";
        }
        return "<a href='https://www.mgvo.de/prog/pub_mitantrag.php?call_id=" . $this->call_id . "' target='_blank' rel='noopener noreferrer'>Mitgliedschaft online beantragen</a>";
    }

    /**
     * Sollte der Mitgliedsantrag außerhalb von Wordpress verwendet werden, müssen die CSS und JS Dateien manuell eingebunden werden.
     * Die Dateien befinden sich im Public Ordner unter im js, bzw. css Ordner.
     * Im Mitantrag Ordner befindet sich eine JSON Konfigurationsdatei, die dazu verwendet werden kann, die Anzeige des Mitantrags zu konfigurieren.
     * Dazu kann die Datei ausgelesen und als Parameter an die Funktion übergeben werden.
     * Wird keine Konfiguration übergeben, wird die Konfiguration aus den MGVO Einstellungen verwendet.
     *
     *
     * @param string $mitantragConfig
     *           JSON Konfiguration als json_decoded string
     * @param boolean $loadMitantragFiles
     *           bool true, wenn die CSS und JS Dateien automatisch eingebunden werden sollen -> benötigt, wenn außerhalb wordpress
     */
    function mgvo_sniplet_mitantrag($mitantragConfig = null, $loadMitantragFiles = true, $formname = "") {
        require_once("mitantrag/MgvoMitgliedsantrag.php");
        $mitantrag = new MgvoMitgliedsantrag($this, $this->call_id, $this->vcryptkey);
        $html = $mitantrag->mgvo_mitantrag($mitantragConfig, $loadMitantragFiles, $formname);

        $this->api->api_printdebug_hidden();
        return $html;
    }

    function mgvo_sniplet_mitgliederbereich_link() {
        if (!check_callid($this->call_id)) {
            return "Keine MGVO CallID angegeben, bitte unter Settings eintragen";
        }
        $sniplet = "<a href='https://www.mgvo.de/mgb/login/login.php?call_id=" . $this->call_id . "' target='_blank' rel='noopener noreferrer'>Login Mitgliederbereich</a>";
        return $sniplet;
    }

    // Hinweis: Um iFrames responsive zu machen, müssen ein paar Vorkehrungen getroffen werden https://www.knothemedia.de/iframe-responsive-gestalten.html
    function mgvo_password_recover_iframe($target = "iframe") {
        if (!check_callid($this->call_id)) {
            return "Keine MGVO Call_ID angegeben, bitte unter Settings eintragen";
        }
        if ($target == "iframe") {
            //$sniplet ="<div><iframe src='https://www.mgvo.de/prog/pub_pwrequest.php?call_id=".$this->call_id."' width='800' height='800px' frameborder='0'>Selbstauskunft kann nicht eingebunden werden</iframe></div><br>";

            $sniplet = <<<JSCRIPT
         
<script type="text/javascript">
function iframeLoadedPW() {
   var iFrameID = document.getElementById('idIframePW');
   
   if(iFrameID) {
         iFrameID.style.height = "400px"; /* Dies kann auch dynamisch berechnet werden */

   }
}
</script>
<div>

JSCRIPT;
            $sniplet .= "<iframe src='https://www.mgvo.de/prog/pub_pwrequest.php?call_id=" . $this->call_id . "' id='idIframePW' onload='iframeLoadedPW()' style='border: none; width:100%; margin: 0 0 0 1%;' allowfullscreen scrolling='no'> Selbstauskunft kann nicht eingebunden werden</iframe></div><br>";
        } else {
            $sniplet = "<a href='https://www.mgvo.de/prog/pub_pwrequest.php?call_id=" . $this->call_id . "' target='" . $target . "' rel='noopener noreferrer'>Zur Passwortrücksetzung</a>";
        }
        return $sniplet;
    }

    function mgvo_sniplet_kartenbuchung_iframe($eventnr, string $target = "iframe"): string {
        // die Funktion zeigt die kartenbuchung an. Entwerder als iframe oder als link mit angegebenen Target (i.d.R. _blank)
        if (!check_callid($this->call_id)) {
            return "Keine MGVO Call_ID angegeben, bitte unter Settings eintragen";
        }
        if ($target == "iframe") {
            $sniplet = "<div><iframe src='https://www.mgvo.de/prog/pub_bookticket1.php?eventnr=" . $eventnr . "&call_id=" . $this->call_id . "' width='800' height='600px' frameborder='0'>Kartenbuchung kann nicht eingebunden werden</iframe></div><br>";
        } else {
            $sniplet = "<a href='https://www.mgvo.de/prog/pub_bookticket1.php?eventnr=" . $eventnr . "&call_id=" . $this->call_id . "' target='" . $target . "' rel='noopener noreferrer'>Zur Kartenbestellung</a>";
        }
        return $sniplet;
    }

    function mgvo_sniplet_filter_events(array $resar, string $startdate = "now", string $enddate = "", int $number = 0): array {
        if ($startdate != "") {
            $start = new DateTime($startdate);
        }
        if ($enddate != "") {
            $end = new DateTime($enddate);
        }
        $count = 0;

        $resar2 = [];
        //$resar = $this->api->read_training_fail();
        foreach ($resar as $key => $resar_element) {
            if ($key != 'objar') {
                $resar2[$key] = $resar_element;
            }
        }

        foreach ($resar['objar'] as $key2 => $element) {
            if ($startdate != "" and strtotime($element['sdat']) < $start) {
                continue;
            }
            if ($enddate != "" and strtotime($element['sdat']) < $end) {
                continue;
            }
            if ($count > $number) {
                continue;
            }
            $resar2['objar'][$key2] = $element;
            return $resar2;
        }
        return [];
    }
}
