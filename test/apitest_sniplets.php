<?php

require_once __DIR__ . "/../libs/MgvoHpApi.php";
require_once __DIR__ . "/../libs/MgvoSniplet.php";

// CALL_ID zur Identifikation des Vereins (hier: Demoverein)
const CALL_ID = "9a052167eb8a71f51b686e35c18a665a";
// Symetrischer Schlüssel, muss identisch sein mit dem Schlüssel, der in den technischen Parametern abgelegt wird.
const VCRYPTKEY = "f4jd8Nzhfr4f8tbhkHGZ765VGVujg";

// Instanziierung der Klasse MgvoSniplet
// Der dritte Parameter sollte unbedingt im Produktivbetrieb auf 5 (Minuten) oder höher eingestellt werden.
$msc = new MgvoSniplet(CALL_ID, VCRYPTKEY, 0);

$msc->set_debuglevel(MGVO_DEBUG_ERR);

$sniplet_no = $_GET['sniplet_no'] ?? 1;

switch ($sniplet_no) {
    case 1:
        $result = $msc->mgvo_sniplet_abteilungen();
        break;
    case 2:
        $result = $msc->mgvo_sniplet_betreuer();
        break;
    case 3:
        $result = $msc->mgvo_sniplet_events();
        break;
    case 4:
        $result = $msc->mgvo_sniplet_gruppen();
        break;
    case 5:
        $result = $msc->mgvo_sniplet_list_documents();
        break;
    //case 6:
    //    $msc->mgvo_sniplet_mitpict(2);
    //    break;
    case 7:
        $result = $msc->mgvo_sniplet_orte();
        break;
    case 8:
        $result = $msc->mgvo_sniplet_read_mitglieder(['suchbeg' => "l*"]);
        break;
    case 9:
        $result = $msc->mgvo_sniplet_show_mitglied(8);
        break;
    case 10:
        $result = $msc->mgvo_sniplet_training_fail();
        break;
    case 11:
        $result = $msc->mgvo_sniplet_vkal(2, 2019);
        break;
    default:
        $result = "Unknown sniplet_no $sniplet_no";
}

echo "<html lang='de'> ";
echo "<body> ";
echo "<div style='display: flex; justify-content: center'> ";
echo $result;
echo "</div> ";
echo "</body> ";
echo "</html> ";
?>