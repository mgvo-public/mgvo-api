<?php

require_once __DIR__ . "/../libs/MgvoHpApi.php";

$mgvo_debug = 0;
$glob_debug = 0;

// CALL_ID zur Identifikation des Vereins (hier: Demoverein)
const CALL_ID = "9a052167eb8a71f51b686e35c18a665a";
// Symetrischer Schlüssel, muss identisch sein mit dem Schlüssel, der in den technischen Parametern abgelegt wird.
const VCRYPTKEY = "f4jd8Nzhfr4f8tbhkHGZ765VGVujg";

header("Cache-Control: no-cache, must-revalidate");
echo "<html><body>";

// Instanziierung der Klasse MGVO_HPAPI
// Der dritte Parameter sollte unbedingt im Produktivbetrieb auf 5 (Minuten) oder höher eingestellt werden.

$akt = date("d.m.Y H:i:s");
echo "<h2>Start Test $akt</h2>";

$hp = new MgvoHpApi(CALL_ID, VCRYPTKEY, 0);

$selmgnr = $_GET['selmgnr'];
$retar = $hp->show_mitglied($selmgnr); 
print_ar($retar);

if (!isset($retar['errno']) || $retar['errno'] == 0) {
    echo $retar['objar']['rettxt'] . "<br>";
} 
else {
    echo "Fehler $retar[errno]: $retar[msg]<br>";
}

echo "Testende";
echo "</body></html>";