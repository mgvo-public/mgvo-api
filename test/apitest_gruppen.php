<?php

require_once __DIR__ . "/../libs/MgvoHpApi.php";

$mgvo_debug = 0;
$glob_debug = 0;

// CALL_ID zur Identifikation des Vereins (hier: Demoverein)
const CALL_ID = "9a052167eb8a71f51b686e35c18a665a";
// Symetrischer Schlüssel, muss identisch sein mit dem Schlüssel, der in den technischen Parametern abgelegt wird.
const VCRYPTKEY = "f4jd8Nzhfr4f8tbhkHGZ765VGVujg";

// Instanziierung der Klasse MgvoHpApi
// Der dritte Parameter sollte unbedingt im Produktivbetrieb auf 5 (Minuten) oder höher eingestellt werden.
$hp = new MgvoHpApi(CALL_ID, VCRYPTKEY, 0);

header("Cache-Control: no-cache, must-revalidate");
?>
<html lang="de">
<body>
<h2>Start Test <?= date("d.m.Y H:i:s") ?></h2>
<pre><?= json_encode($hp->read_gruppen(), JSON_PRETTY_PRINT) ?></pre>
</body>
</html>
