<?php

require_once __DIR__ . "/../libs/MgvoHpApi.php";

$mgvo_debug = 0;
$glob_debug = 0;

// CALL_ID zur Identifikation des Vereins (hier: Demoverein)
const CALL_ID = "9a052167eb8a71f51b686e35c18a665a";
// Symetrischer Schlüssel, muss identisch sein mit dem Schlüssel, der in den technischen Parametern abgelegt wird.
const VCRYPTKEY = "f4jd8Nzhfr4f8tbhkHGZ765VGVujg";

// Instanziierung der Klasse MgvoHpApi
// Der dritte Parameter sollte unbedingt im Produktivbetrieb auf 5 (Minuten) oder höher eingestellt werden.

$akt = date("d.m.Y H:i:s");
echo "<h2>Start Test $akt</h2>";

$hp = new MgvoHpApi(CALL_ID, VCRYPTKEY, 0);
$retar = $hp->create_mitstamm([
    "nachname" => "Müller-Lüdenscheid",
    "vorname" => "Justin-Kevin",
    "zahlweise" => "j",
    "zahlungsart" => "l",
    "anrede" => 1,
    "geschlecht" => "m",
    "ort" => "Berlin",
    "plz" => 10365,
    "str" => "Gernotstr. 12",
    "notiz" => "Ganz netter Kerl\nmanchmal besoffen"
]);

$result = !isset($retar['errno']) || $retar['errno'] == 0 ? $retar['objar']['rettxt'] : "Fehler $retar[errno]: $retar[msg]";

header("Cache-Control: no-cache, must-revalidate");
?>
<html lang="de">
<body>
<h2>Start Test <?= date("d.m.Y H:i:s") ?></h2>
<pre><?= json_encode($retar, JSON_PRETTY_PRINT) ?></pre>
<?= $result ?><br>
</body>
</html>
